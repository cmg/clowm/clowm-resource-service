#! /usr/bin/env bash
set -e

./prestart.sh

# Start webserver
uvicorn app.main:app --host 0.0.0.0 --port 8000 --no-server-header
