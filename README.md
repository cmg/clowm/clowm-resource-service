# 🚨 Deprecation 🚨
This repository was merged into the [CloWM backend](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-backend) and will NOT be developed further.  
Everything is in read-only mode.


# CloWM Resource Service

## Description

This is the Resource service of the CloWM service.

## Configuration

### General

| Env variable             | Config file key | Default       | Value    | Example                | Description                                                                                                           |
|--------------------------|-----------------|---------------|----------|------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `CLOWM_CONFIG_FILE_YAML` | -               | `config.yaml` | Filepath | `/path/to/config.yaml` | Path to a YAML file to read the config. See [example-config/example-config.yaml](example-config/example-config.yaml). |
| `CLOWM_CONFIG_FILE_TOML` | -               | `config.toml` | Filepath | `/path/to/config.toml` | Path to a TOML file to read the config. See [example-config/example-config.toml](example-config/example-config.toml). |
| `CLOWM_CONFIG_FILE_JSON` | -               | `config.json` | Filepath | `/path/to/config.json` | Path to a JSON file to read the config. See [example-config/example-config.json](example-config/example-config.json). |
| `CLOWM_API_PREFIX`       | `api_prefix`    | unset         | URI path | `/api`                 | Prefix before every URL path                                                                                          |

### Database

| Env variable           | Config file key | Default     | Value              | Example       | Description                                                    |
|------------------------|-----------------|-------------|--------------------|---------------|----------------------------------------------------------------|
| `CLOWM_DB__HOST`       | `db.host`       | `localhost` | <db hostname / IP> | `localhost`   | IP or Hostname Address of DB                                   |
| `CLOWM_DB__PORT`       | `db.port`       | 3306        | Integer            | 3306          | Port of the database                                           |
| * `CLOWM_DB__USER`     | `db.user`       | unset       | String             | `db-user`     | Username of the database user                                  |
| * `CLOWM_DB__PASSWORD` | `db.password`   | unset       | String             | `db-password` | Password of the database user                                  |
| * `CLOWM_DB__NAME`     | `db.name`       | unset       | String             | `db-name`     | Name of the database                                           |
| `CLOWM_DB__VERBOSE`    | `db.verbose`    | `false`     | Boolean            | `false`       | Enables verbose SQL output.<br>Should be `false` in production |

### Email

| Variable                          | Config file key            | Default              | Value                   | Example              | Description                                                                                                |
|-----------------------------------|----------------------------|----------------------|-------------------------|----------------------|------------------------------------------------------------------------------------------------------------|
| `CLOWM_SMTP__SERVER`              | `smtp.server`              | unset                | SMTP domain / `console` | `localhost`          | Hostname of SMTP server. If `console`, emails are printed to the console. If not set, emails are not sent. |
| `CLOWM_SMTP__PORT`                | `smtp.port`                | 587                  | Integer                 | 587                  | Port of the SMTP server                                                                                    |
| `CLOWM_SMTP__SENDER_EMAIL`        | `smtp.sender_email`        | `no-reply@clowm.com` | Email                   | `no-reply@clowm.com` | Email address from which the emails are sent.                                                              |
| `CLOWM_SMTP__REPLY_EMAIL`         | `smtp.reply_email`         | unset                | Email                   | `clowm@example.org`  | Email address in the `Reply-To` header.                                                                    |
| `CLOWM_SMTP__CONNECTION_SECURITY` | `smtp.connection_security` | unset                | `starttls` / `ssl`      | `starttls`           | Connection security to the SMTP server.                                                                    |
| `CLOWM_SMTP__LOCAL_HOSTNAME`      | `smtp.local_hostname`      | unset                | String                  | `clowm.local`        | Overwrite the local hostname from which the emails are sent.                                               |
| `CLOWM_SMTP__CA_PATH`             | `smtp.ca_path`             | unset                | Filepath                | `/path/to/ca.pem`    | Path to a custom CA certificate.                                                                           |
| `CLOWM_SMTP__KEY_PATH`            | `smtp.key_path`            | unset                | Filepath                | `/path/to/key.pem`   | Path to the CA key.                                                                                        |
| `CLOWM_SMTP__USER`                | `smtp.user`                | unset                | String                  | `smtp-user`          | Username to use for SMTP login.                                                                            |
| `CLOWM_SMTP__PASSWORD`            | `smtp.password`            | unset                | String                  | `smtp-password`      | Password to use for SMTP login.                                                                            |

### S3

| Env variable                | Config file key      | Default           | Value    | Example                  | Description                                 |
|-----------------------------|----------------------|-------------------|----------|--------------------------|---------------------------------------------|
| * `CLOWM_S3__URI`           | `s3.uri`             | unset             | HTTP URL | `http://localhost`       | URI of the S3 Object Storage                |
| * `CLOWM_S3__ACCESS_KEY`    | `s3.acess_key`       | unset             | String   | `ZR7U56KMK20VW`          | Access key for the S3 that owns the buckets |
| * `CLOWM_S3__SECRET_KEY`    | `s3.secret_key`      | unset             | String   | `9KRUU41EGSCB3H9ODECNHW` | Secret key for the S3 that owns the buckets |
| `CLOWM_S3__RESOURCE_BUCKET` | `s3.resource_bucket` | `clowm-resources` | String   | `clowm-resources`        | Bucket where to save the resources.         |

### Security

| Env variable                                   | Config file key                  | Default | Value                           | Example            | Description                                         |
|------------------------------------------------|----------------------------------|---------|---------------------------------|--------------------|-----------------------------------------------------|
| * `CLOWM_PUBLIC_KEY` / `CLOWM_PUBLIC_KEY_FILE` | `public_key` / `public_key_file` | unset   | Public Key / Path to Public Key | `/path/to/key.pub` | Public part of RSA Key in PEM format to verify JWTs |
| * `CLOWM_OPA__URI`                             | `opa.uri`                        | unset   | HTTP URL                        | `http://localhost` | URI of the OPA Service                              |

### Cluster

| Env variable                             | Config file key             | Default               | Value                 | Example               | Description                                                                                       |
|------------------------------------------|-----------------------------|-----------------------|-----------------------|-----------------------|---------------------------------------------------------------------------------------------------|
| * `CLOWM_CLUSTER__SLURM__URI`            | `cluster.slurm.uri`         | unset                 | HTTP Url              | `http://localhost`    | HTTP URL to communicate with the Slurm cluster                                                    |
| * `CLOWM_CLUSTER__SLURM__TOKEN`          | `cluster.slurm.token`       | unset                 | String                | -                     | JWT for communication with the Slurm REST API.                                                    |
| `CLOWM_CLUSTER__SLURM__USER`             | `cluster.slurm.user`        | `slurm`               | String                | `slurm`               | User on the slurm cluster who should run the job. Should be the user of the `cluster.slurm.token` |
| `CLOWM_CLUSTER__WORKING_DIRECTORY`       | `cluster.working_directory` | `/tmp`                | Path on slurm cluster | `/path/to/directory`  | Working directory for the slurm jobs                                                              |
| `CLOWM_CLUSTER__RESOURCE_CLUSTER_PATH`   | `cluster.execution_cleanup` | `/vol/data/databases` | Path on slurm cluster | `/vol/data/databases` | Directory on the cluster where the resources should be downloaded to                              |
| `CLOWM_CLUSTER__RESOURCE_CONTAINER_PATH` | `cluster.execution_cleanup` | `/vol/resources`      | Path                  | `/vol/resources`      | Directory in the container where al resources are readonly available                              |

### Monitoring

| Env variable                | Config file key      | Default | Value   | Example     | Description                                                                                  |
|-----------------------------|----------------------|---------|---------|-------------|----------------------------------------------------------------------------------------------|
| `CLOWM_OTLP__GRPC_ENDPOINT` | `otlp.grpc_endpoint` | unset   | String  | `localhost` | OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger. If unset, no traces are sent. |
| `CLOWM_OTLP__SECURE`        | `otlp.secure`        | `false` | Boolean | `false`     | Connection type                                                                              |

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information.
