#!/bin/sh -e
set -x

isort --force-single-line-imports app
ruff format app
ruff check --fix --show-fixes app
isort app
