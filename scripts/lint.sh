#!/usr/bin/env bash

set -x

ruff --version
ruff check app
ruff format --diff app

isort --version
isort -c app

mypy --version
mypy app
