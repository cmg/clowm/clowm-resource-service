#! /usr/bin/env bash

# Check Connection to Ceph RGW
python app/check_s3_connection.py
# Check Connection to Slurm Cluster
python app/check_slurm_connection.py
# Let the DB start
python app/check_database_connection.py
