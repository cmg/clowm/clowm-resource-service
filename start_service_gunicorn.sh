#! /usr/bin/env sh
set -e

./prestart.sh

# Start Gunicorn
exec gunicorn -k uvicorn.workers.UvicornWorker -c /app/gunicorn_conf.py app.main:app
