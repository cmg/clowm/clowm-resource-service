import logging

from clowmdb import latest_revision
from clowmdb.db.session import get_session
from sqlalchemy import text
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 3  # 3 minutes
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    try:
        with get_session(url=str(settings.db.dsn_sync)) as db:
            # Try to create session to check if DB is awake
            db_revision = db.execute(text("SELECT version_num FROM alembic_version LIMIT 1")).scalar_one_or_none()
            if db_revision != latest_revision:
                raise ValueError(
                    f"Database revision doesn't match revision defined by package `clowmdb`. Expected {latest_revision}, found {db_revision}"  # noqa:E501
                )
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Initializing DB")
    init()
    logger.info("DB finished initializing")


if __name__ == "__main__":
    main()
