from enum import Enum, unique

from fastapi import status
from httpx import AsyncClient
from opentelemetry import trace

from app.core.config import settings
from app.slurm.schemas import SlurmJobSubmission

tracer = trace.get_tracer_provider().get_tracer(__name__)


class SlurmClient:
    @unique
    class JobState(str, Enum):
        """
        Enumeration for the possible states of a slurm job.
        """

        RUNNING: str = "RUNNING"
        SUCCESS: str = "SUCCESS"
        ERROR: str = "ERROR"

    def __init__(self, client: AsyncClient):
        """
        Initialize the client to communicate with a Slurm cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            Async HTTP client with an open connection.
        """
        self._client = client
        self._headers = {
            "X-SLURM-USER-TOKEN": settings.cluster.slurm.token.get_secret_value(),
            "X-SLURM-USER-NAME": settings.cluster.slurm.user,
        }

    async def submit_job(self, job_submission: SlurmJobSubmission) -> int:
        """
        Submit a job to the slurm cluster.

        Parameters
        ----------
        job_submission : app.slurm.schemas.SlurmJobSubmission
            JOb with properties to execute on the cluster.

        Returns
        -------
        slurm_job_id : int
            Slurm job ID of submitted job.
        """
        with tracer.start_as_current_span(
            "slurm_submit_job",
            attributes={
                "parameters": job_submission.job.model_dump_filtered_json(exclude_none=True),
                "job_script": job_submission.script,
            },
        ) as span:
            response = await self._client.post(
                f"{settings.cluster.slurm.uri}slurm/v0.0.38/job/submit",
                headers={"Content-Type": "application/json", **self._headers},
                content=job_submission.model_dump_json(exclude_none=True),
            )
            span.set_attribute("response_status_code", response.status_code)
            if response.status_code != status.HTTP_200_OK:
                raise KeyError("Error at slurm")
            job_id = int(response.json()["job_id"])
            span.set_attribute("job_id", job_id)
            return job_id

    async def job_state(self, job_id: int) -> JobState:
        """
        Check the current state of a slurm job

        Parameters
        ----------
        job_id : int
            ID of the job to cancel.

        Returns
        -------
        finished : SlurmClient.JobState
            The state if the specified job

        Notes
        -----
        If the job is not found, then the function returns SUCCESS
        """
        with tracer.start_as_current_span("slurm_check_job_status") as span:
            response = await self._client.get(
                f"{settings.cluster.slurm.uri}slurm/v0.0.38/job/{job_id}", headers=self._headers
            )
            span.set_attribute("slurm.job-status.request.code", response.status_code)
            if response.status_code != status.HTTP_200_OK:  # pragma: no cover
                return SlurmClient.JobState.SUCCESS
            try:
                job_state = response.json()["jobs"][0]["job_state"]
                span.set_attribute("slurm.job-status.state", job_state)
                if job_state == "COMPLETED":
                    return SlurmClient.JobState.SUCCESS
                elif job_state in ["FAILED", "CANCELLED"]:
                    return SlurmClient.JobState.ERROR
                return SlurmClient.JobState.RUNNING
            except (KeyError, IndexError) as ex:  # pragma: no cover
                span.record_exception(ex)
                return SlurmClient.JobState.ERROR
