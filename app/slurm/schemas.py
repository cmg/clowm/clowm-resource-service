from typing import Any
from uuid import uuid4

from pydantic import BaseModel, Field


class SlurmJobProperties(BaseModel):
    current_working_directory: str = Field(
        "/tmp", description="Instruct Slurm to connect the batch script's standard output directly to the file name."
    )
    environment: dict[str, Any] = Field(default_factory=lambda: {}, description="Dictionary of environment entries.")
    name: str = Field(default_factory=lambda: uuid4().hex, description="Specify a name for the job allocation.")
    requeue: bool = Field(False, description="Specifies that the batch job should eligible to being requeue.")
    standard_output: str | None = Field(
        None, description="Instruct Slurm to connect the batch script's standard output directly to the file name."
    )
    argv: list[str] | None = Field(None, description="Arguments to the script.")

    def model_dump_filtered_json(self, *args: Any, **kwargs: Any) -> str:
        filtered_environment = {k: v for k, v in self.environment.items() if "KEY" not in k}
        filtered_model = self.model_copy(update={"environment": filtered_environment})
        return filtered_model.model_dump_json(*args, **kwargs)


class SlurmJobSubmission(BaseModel):
    script: str = Field(..., description="Executable script (full contents) to run in batch step")
    job: SlurmJobProperties = Field(...)
