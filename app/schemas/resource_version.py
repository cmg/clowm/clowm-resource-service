from typing import Literal

from clowmdb.models import ResourceVersion
from pydantic import BaseModel, Field, computed_field, model_validator

from app.core.config import settings
from app.schemas import UUID


class FileTree(BaseModel):
    type: Literal["file", "directory", "link"]
    name: str
    target: str | None = None
    contents: list["FileTree"] | None = None
    size: int


class UserSynchronizationRequestIn(BaseModel):
    reason: str = Field(
        default=...,
        description="Reason why the request was requested.",
        min_length=16,
        max_length=512,
        examples=["This version is required to reproduce a execution with Workflow XY@2.0."],
    )


class UserSynchronizationRequestOut(UserSynchronizationRequestIn):
    resource_version_id: UUID = Field(
        ..., description="ID of the resource version", examples=["fb4cee12-1e91-49f3-905f-808845c7c1f4"]
    )
    resource_id: UUID = Field(..., description="ID of the resource", examples=["4c072e39-2bd9-4fa3-b564-4d890e240ccd"])
    requester_id: UUID = Field(
        ...,
        description="ID of the user that requested this resource synchronization",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )

    @staticmethod
    def from_db_resource_version(db_resource_version: ResourceVersion) -> "UserSynchronizationRequestOut":
        return UserSynchronizationRequestOut(
            reason=db_resource_version.synchronization_request_description,
            resource_version_id=db_resource_version.resource_version_id,
            resource_id=db_resource_version.resource_id,
            requester_id=db_resource_version.synchronization_request_uid,
        )


class UserRequestAnswer(BaseModel):
    deny: bool = Field(default=False, description="Flag to indicate if the request was denied.")
    reason: str | None = Field(
        default=None,
        min_length=16,
        max_length=512,
        description="Reason why the request was denied or approved. Required if request is denied.",
    )

    @model_validator(mode="after")
    def check_reason_field(self) -> "UserRequestAnswer":
        if self.deny and self.reason is None:
            raise ValueError("reason can't be empty if deny is true")
        return self


class BaseResourceVersion(BaseModel):
    release: str = Field(
        ...,
        description="Short tag describing the version of the resource",
        examples=["01-2023"],
        min_length=3,
        max_length=32,
    )


class ResourceVersionIn(BaseResourceVersion):
    pass


class ResourceVersionOut(BaseResourceVersion):
    status: ResourceVersion.Status = Field(..., description="Status of the resource version")
    resource_version_id: UUID = Field(
        ..., description="ID of the resource version", examples=["fb4cee12-1e91-49f3-905f-808845c7c1f4"]
    )
    resource_id: UUID = Field(..., description="ID of the resource", examples=["4c072e39-2bd9-4fa3-b564-4d890e240ccd"])

    created_at: int = Field(
        ..., description="Timestamp when the version was created as UNIX timestamp", examples=[1672527600]
    )

    @computed_field(  # type: ignore[misc]
        description="Path to the resource on the cluster if the resource is synchronized",
        examples=[
            "/vol/resources/CLDB-0e240ccd/fb4cee121e9149f3905f808845c7c1f4",
            "/vol/resources/CLDB-0e240ccd/latest",
        ],
    )
    @property
    def cluster_path(self) -> str:
        return f"{settings.cluster.resource_container_path}/{resource_dir_name(self.resource_id)}/{self.resource_version_id.hex if self.status != ResourceVersion.Status.LATEST else 'latest'}"

    @computed_field(  # type: ignore[misc]
        description="Path to the resource in the S3 Bucket. Not publicly available.",
        examples=[
            "s3://clowm-resources/CLDB-0e240ccd/fb4cee121e9149f3905f808845c7c1f4/resource.tar.gz",
        ],
    )
    @property
    def s3_path(self) -> str:
        return f"s3://{settings.s3.resource_bucket}/{resource_version_key(self.resource_id, self.resource_version_id)}"

    @staticmethod
    def from_db_resource_version(db_resource_version: ResourceVersion) -> "ResourceVersionOut":
        return ResourceVersionOut(
            release=db_resource_version.release,
            status=db_resource_version.status,
            resource_version_id=db_resource_version.resource_version_id,
            resource_id=db_resource_version.resource_id,
            created_at=db_resource_version.created_at,
        )


def resource_dir_name(resource_id: UUID) -> str:
    return f"CLDB-{resource_id.hex}"


def resource_version_dir_name(resource_id: UUID, resource_version_id: UUID) -> str:
    return resource_dir_name(resource_id) + f"/{resource_version_id.hex}"


def resource_version_key(resource_id: UUID, resource_version_id: UUID) -> str:
    return resource_version_dir_name(resource_id, resource_version_id) + "/resource.tar.gz"
