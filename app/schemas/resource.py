from typing import Sequence

from clowmdb.models import Resource, ResourceVersion, User
from pydantic import BaseModel, Field

from app.schemas import UUID
from app.schemas.resource_version import ResourceVersionIn, ResourceVersionOut, resource_version_dir_name


class BaseResource(BaseModel):
    name: str = Field(..., description="Short Name for the resource", min_length=3, max_length=32, examples=["blastdb"])
    description: str = Field(
        ...,
        description="Short description for this resource",
        min_length=16,
        max_length=264,
        examples=["This is a short description for a resource"],
    )
    source: str = Field(
        ...,
        description="A link or similar where the resource originates from",
        min_length=8,
        max_length=264,
        examples=["https://example.com/db"],
    )
    private: bool = Field(default=True, description="Flag if this resource should be default visible in the UI")


class ResourceIn(BaseResource, ResourceVersionIn):
    pass


class ResourceOut(BaseResource):
    resource_id: UUID = Field(..., description="ID of the resource", examples=["4c072e39-2bd9-4fa3-b564-4d890e240ccd"])
    maintainer_id: UUID = Field(
        ..., description="ID of the maintainer", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]
    )
    versions: list[ResourceVersionOut] = Field(..., description="Versions of the resource")

    @staticmethod
    def from_db_resource(db_resource: Resource, versions: Sequence[ResourceVersion] | None = None) -> "ResourceOut":
        """
        Create a ResourceOut schema from the resource database model.

        Parameters
        ----------
        db_resource: clowmdb.models.Resource
            Database model of a resource.
        versions : list[clowmdb.models.ResourceVersion] | None, default None
            List of versions to attach to the schema. If None, they will be loaded from the DB model.

        Returns
        -------
        resource : ResourceOut
            Schema from the database model
        """
        if versions is not None:
            temp_versions = versions
        else:
            temp_versions = db_resource.versions
        return ResourceOut(
            resource_id=db_resource.resource_id,
            name=db_resource.name,
            description=db_resource.short_description,
            source=db_resource.source,
            maintainer_id=db_resource.maintainer_id,
            private=db_resource.private,
            versions=[ResourceVersionOut.from_db_resource_version(v) for v in temp_versions],
        )


class S3ResourceVersionInfo(BaseResource):
    release: str = Field(
        ...,
        description="Short tag describing the version of the resource",
        examples=["01-2023"],
    )
    resource_id: UUID = Field(..., description="ID of the resource", examples=["4c072e39-2bd9-4fa3-b564-4d890e240ccd"])
    resource_version_id: UUID = Field(
        ..., description="ID of the resource version", examples=["fb4cee12-1e91-49f3-905f-808845c7c1f4"]
    )
    maintainer_id: UUID = Field(..., description="ID of the maintainer", examples=["28c5353b8bb34984a8bd4169ba94c606"])
    maintainer: str = Field(..., description="Name of the maintainer", examples=["Bilbo Baggins"])

    def s3_path(self) -> str:
        return resource_version_dir_name(self.resource_id, self.resource_version_id) + "/clowm_resinfo.json"

    @staticmethod
    def from_models(resource: Resource, resource_version: ResourceVersion, maintainer: User) -> "S3ResourceVersionInfo":
        return S3ResourceVersionInfo(
            name=resource.name,
            description=resource.short_description,
            source=resource.source,
            resource_id=resource.resource_id,
            resource_version_id=resource_version.resource_version_id,
            maintainer=maintainer.display_name,
            maintainer_id=maintainer.uid,
            release=resource_version.release,
        )
