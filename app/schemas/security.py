from datetime import datetime
from enum import StrEnum, unique
from typing import Any

from pydantic import BaseModel, Field

from app.schemas import UUID


@unique
class Roles(StrEnum):
    USER: str = "user"
    ADMINISTRATOR: str = "administrator"
    DEVELOPER = "developer"
    REVIEWER = "reviewer"
    DB_MAINTAINER: str = "db_maintainer"


class OPARequest(BaseModel):
    opa_path: str = Field(
        ...,
        description="URL path where OPA is queried",
        exclude=True,
        examples=["/v1/data/clowm/authz/allow"],
    )
    input: Any


class OPAResponse(BaseModel):
    decision_id: UUID = Field(
        ...,
        description="Decision ID for for the specific decision",
        examples=["8851dce0-7546-4e81-a89d-111cbec376c1"],
    )
    result: Any


class AuthzResponse(OPAResponse):
    """Schema for a response from OPA"""

    result: bool = Field(..., description="Result of the Authz request")


class AuthzRequest(BaseModel):
    """Schema for a Request to OPA"""

    uid: str = Field(..., description="UID of user", examples=["28c5353b8bb34984a8bd4169ba94c606"])
    operation: str = Field(..., description="Operation the user wants to perform", examples=["read"])
    resource: str = Field(..., description="Resource the operation should be performed on", examples=["bucket"])


class RoleUsersRequest(BaseModel):
    roles: list[Roles] = Field(
        ..., description="List of roles that you want the users of", min_length=1, max_length=len(Roles)
    )


class _RoleDict(BaseModel):
    roles: dict[str, list[str]] = Field(..., description="Mapping from role to lifescience ids")


class RoleUsersResponse(OPAResponse):
    result: _RoleDict


class JWT(BaseModel):
    """
    Schema for a JWT. Only for convenience
    """

    exp: datetime
    sub: str
    raw_token: str


class ErrorDetail(BaseModel):
    """
    Schema for a error due to a rejected request.
    """

    detail: str = Field(..., description="Detail about the occurred error")
