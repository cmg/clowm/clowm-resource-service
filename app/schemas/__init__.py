from typing import Annotated
from uuid import UUID as NativeUUID

from pydantic import PlainSerializer

UUID = Annotated[NativeUUID, PlainSerializer(lambda uuid: str(uuid), return_type=str, when_used="unless-none")]
