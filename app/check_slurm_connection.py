import logging

import httpx
from fastapi import status
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 3  # 3 minutes
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    headers = {
        "X-SLURM-USER-TOKEN": settings.cluster.slurm.token.get_secret_value(),
        "X-SLURM-USER-NAME": settings.cluster.slurm.user,
    }

    try:
        response = httpx.get(
            f"{settings.cluster.slurm.uri}slurm/v0.0.38/ping", timeout=5.0, follow_redirects=False, headers=headers
        )
        assert response.status_code == status.HTTP_200_OK
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Check Slurm Cluster connection")
    init()
    logger.info("Slurm Cluster connection established")


if __name__ == "__main__":
    main()
