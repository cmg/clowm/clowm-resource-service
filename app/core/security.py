from typing import Type, TypeVar

from authlib.jose import JsonWebToken
from fastapi import HTTPException, status
from httpx import AsyncClient
from opentelemetry import trace

from app.core.config import settings
from app.schemas.security import (
    AuthzRequest,
    AuthzResponse,
    OPARequest,
    OPAResponse,
    Roles,
    RoleUsersRequest,
    RoleUsersResponse,
)

T = TypeVar("T", bound=OPAResponse)

ISSUER = "clowm"
ALGORITHM = "RS256"
jwt = JsonWebToken([ALGORITHM])

tracer = trace.get_tracer_provider().get_tracer(__name__)


def decode_token(token: str) -> dict[str, str]:  # pragma: no cover
    """
    Decode and verify a JWT token.

    Parameters
    ----------
    token : str
        The JWT to decode.

    Returns
    -------
    decoded_token : dict[str, str]
        Payload of the decoded token.
    """
    claims = jwt.decode(
        s=token,
        key=settings.public_key_value.get_secret_value(),
        claims_options={
            "iss": {"essential": True},
            "sub": {"essential": True},
            "exp": {"essential": True},
        },
    )
    claims.validate()
    return claims


async def _request_opa(request: OPARequest, response_type: Type[T], *, client: AsyncClient) -> T:
    """
    Wrapper to send a generic request to OPA.

    Parameters
    ----------
    request : OPARequest
        The request for OPA.
    response_type : Type[OPAResponse -> T]
        The class to which the response will be parsed. T must inherit from OPAResponse.
    client : httpx.AsyncClient
        An async http client with an open connection.

    Returns
    -------
    response : T
        The parsed response from OPA.
    """
    with tracer.start_as_current_span(
        "opa_request",
        attributes={"opa_path": request.opa_path.strip("/"), "body": request.model_dump_json(indent=4)},
    ) as span:
        response = await client.post(
            str(settings.opa.uri) + request.opa_path.strip("/"),
            content=request.model_dump_json(),
            headers={"content-type": "application/json"},
        )
        parsed_response = response_type.model_validate_json(response.content)
        span.set_attribute("decision_id", str(parsed_response.decision_id))
        return parsed_response


async def request_authorization(request_params: AuthzRequest, client: AsyncClient) -> AuthzResponse:
    """
    Send a request to OPA for a policy decision. Raise an HTTPException with status 403 if authorization is denied.

    Parameters
    ----------
    request_params : app.schemas.security.AuthRequest
        Input parameters for the authorization request.
    client : httpx.AsyncClient
        An async http client with an open connection. This function doesn't close the connection afterwards.

    Returns
    -------
    response : app.schemas.security.AuthzResponse
        Response by the Auth service about the authorization request
    """

    with tracer.start_as_current_span(
        "authorization",
        attributes={
            "resource": request_params.resource,
            "operation": request_params.operation,
            "uid": request_params.uid,
        },
    ):
        response = await _request_opa(
            request=OPARequest(input=request_params, opa_path="/v1/data/clowm/authz/allow"),
            response_type=AuthzResponse,
            client=client,
        )
    if not response.result:  # pragma: no cover
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Action forbidden. Decision ID {response.decision_id}",
        )
    return response


async def request_role_users(roles: list[Roles], client: AsyncClient) -> RoleUsersResponse:
    """
    Send a request to OPA to get the all users with the specified roles.

    Parameters
    ----------
    roles : list[app.schemas.security.Roles]
        List of roles.
    client : httpx.AsyncClient
        An async http client with an open connection. This function doesn't close the connection afterwards.

    Returns
    -------
    response : app.schemas.security.RoleUsersResponse
        Response with all lifescience ids per role
    """

    with tracer.start_as_current_span(
        "request_role_users",
        attributes={
            "roles": [str(r) for r in roles],
        },
    ):
        return await _request_opa(
            request=OPARequest(input=RoleUsersRequest(roles=roles), opa_path="/v1/data/clowm/role_users"),
            response_type=RoleUsersResponse,
            client=client,
        )
