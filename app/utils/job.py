from inspect import iscoroutinefunction
from typing import Callable, Generic, ParamSpec, TypeVar

P = ParamSpec("P")
T = TypeVar("T")


class Job(Generic[P, T]):
    def __init__(self, func: Callable[P, T], *args: P.args, **kwargs: P.kwargs) -> None:
        self.func = func
        self.args = args
        self.kwargs = kwargs

    @property
    def is_async(self) -> bool:
        return iscoroutinefunction(self.func)

    def __call__(self) -> T:
        return self.func(*self.args, **self.kwargs)


class AsyncJob(Job):
    def __init__(self, func: Callable[P, T], *args: P.args, **kwargs: P.kwargs) -> None:
        super().__init__(func, *args, **kwargs)
        assert iscoroutinefunction(self.func)

    async def __call__(self) -> T:
        return await super().__call__()
