from typing import Any

from fastapi import APIRouter, Depends, status

from app.api.dependencies import decode_bearer_token
from app.api.endpoints import resource_version, resources
from app.schemas.security import ErrorDetail

alternative_responses: dict[int | str, dict[str, Any]] = {
    status.HTTP_400_BAD_REQUEST: {
        "model": ErrorDetail,
        "description": "Error in request parameters",
        "content": {"application/json": {"example": {"detail": "Malformed JWT Token"}}},
    },
    status.HTTP_401_UNAUTHORIZED: {
        "model": ErrorDetail,
        "description": "Not authenticated",
        "content": {"application/json": {"example": {"detail": "Not authenticated"}}},
    },
    status.HTTP_403_FORBIDDEN: {
        "model": ErrorDetail,
        "description": "Not authorized",
        "content": {"application/json": {"example": {"detail": "Not authorized"}}},
    },
    status.HTTP_404_NOT_FOUND: {
        "model": ErrorDetail,
        "description": "Entity not Found",
        "content": {"application/json": {"example": {"detail": "Entity not found."}}},
    },
}

api_router = APIRouter()
api_router.include_router(
    resources.router,
    dependencies=[Depends(decode_bearer_token)],
    responses=alternative_responses,
)
api_router.include_router(
    resource_version.router, dependencies=[Depends(decode_bearer_token)], responses=alternative_responses
)
