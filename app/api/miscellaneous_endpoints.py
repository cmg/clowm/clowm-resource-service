from fastapi import APIRouter, status

miscellaneous_router = APIRouter(include_in_schema=False)


@miscellaneous_router.get(
    "/health",
    tags=["Miscellaneous"],
    responses={
        status.HTTP_200_OK: {
            "description": "Service Health is OK",
            "content": {"application/json": {"example": {"status": "OK"}}},
        },
    },
)
def health_check() -> dict[str, str]:
    """
    Check if the service is reachable.
    \f

    Returns
    -------
    response : dict[str, str]
        status ok
    """
    return {"status": "OK"}
