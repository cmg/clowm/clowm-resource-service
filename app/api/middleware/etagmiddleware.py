from hashlib import md5
from typing import Awaitable, Callable, Mapping

from fastapi import Request, Response, status
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware


class HashJSONResponse(JSONResponse):
    def init_headers(self, headers: Mapping[str, str] | None = None) -> None:
        super().init_headers(headers=headers)
        # Add the ETag header (MD5 hash of content) to the response
        if self.status_code == status.HTTP_200_OK:
            self.headers["ETag"] = md5(self.body).hexdigest()


class ETagMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: Callable[[Request], Awaitable[Response]]) -> Response:
        response = await call_next(request)
        if request.method == "GET":
            # Client can ask if the cached data is stale or not
            # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-None-Match
            # This saves network bandwidth but the database is still queried
            if (
                response.headers.get("ETag") is not None
                and request.headers.get("If-None-Match") == response.headers["ETag"]
            ):
                return Response(status_code=status.HTTP_304_NOT_MODIFIED)
        return response
