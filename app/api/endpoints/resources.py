from typing import Annotated, Any, Awaitable, Callable
from uuid import UUID

from clowmdb.models import ResourceVersion
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Query, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema

from app.api.background.cluster import delete_cluster_resource
from app.api.background.s3 import (
    add_s3_resource_version_info,
    delete_resource_policy_stmt,
    delete_s3_resource,
    give_permission_to_s3_resource,
    give_permission_to_s3_resource_version,
)
from app.api.dependencies import AuthorizationDependency, CurrentResource, CurrentUser, DBSession
from app.crud import CRUDResource
from app.schemas.resource import ResourceIn, ResourceOut, S3ResourceVersionInfo
from app.schemas.resource_version import UserSynchronizationRequestOut
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/resources", tags=["Resource"])
resource_authorization = AuthorizationDependency(resource="resource")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(resource_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", summary="List resources")
@start_as_current_span_async("api_list_resources", tracer=tracer)
async def list_resources(
    authorization: Authorization,
    current_user: CurrentUser,
    db: DBSession,
    maintainer_id: Annotated[
        UUID | SkipJsonSchema[None],
        Query(
            description="Filter for resource by maintainer. If current user is the same as maintainer ID, permission `resource:list` required, otherwise `resource:list_filter`.",  # noqa: E501
        ),
    ] = None,
    version_status: Annotated[
        list[ResourceVersion.Status] | SkipJsonSchema[None],
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:list_filter` required if None or querying for non-public resources, otherwise only permission `resource:list` required.",  # noqa: E501
        ),
    ] = None,
    name_substring: Annotated[
        str | SkipJsonSchema[None], Query(max_length=32, description="Filter resources by a substring in their name.")
    ] = None,
    public: Annotated[bool | SkipJsonSchema[None], Query(description="Filter resources to by the public flag")] = None,
) -> list[ResourceOut]:
    """
    List all resources.

    Permission `resource:list` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    maintainer_id : uuid.UUID | None, default None
        Filter resources by a maintainer. Query Parameter.
    version_status : list[clowmdb.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.
    name_substring : str | None, default None
        Filter resources by a substring in their name. Query Parameter.
    public : bool | None, default None
      Filter resources to include only public resources. Query Parameters.
    """
    current_span = trace.get_current_span()
    if maintainer_id:  # pragma: no cover
        current_span.set_attribute("maintainer_id", str(maintainer_id))
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    if name_substring:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if public is not None:  # pragma: no cover
        current_span.set_attribute("public", public)

    rbac_operation = "list"
    if maintainer_id is not None:
        if current_user.uid != maintainer_id:
            rbac_operation = "list_filter"
    # if status is None or contains non-public resources
    elif version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0:
        rbac_operation = "list_filter"
    await authorization(rbac_operation)
    resources = await CRUDResource.list_resources(
        db, name_substring=name_substring, maintainer_id=maintainer_id, version_status=version_status, public=public
    )
    return [ResourceOut.from_db_resource(resource) for resource in resources]


@router.get("/sync_requests", summary="List resource sync requests")
@start_as_current_span_async("api_list_resources_sync_requests", tracer=tracer)
async def list_sync_requests(
    authorization: Authorization,
    db: DBSession,
) -> list[UserSynchronizationRequestOut]:
    """
    List all resource sync requests.

    Permission `resource:sync` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    """
    await authorization("sync")
    resources = await CRUDResource.list_resources(db, version_status=[ResourceVersion.Status.SYNC_REQUESTED])
    return [
        UserSynchronizationRequestOut.from_db_resource_version(version)
        for resource in resources
        for version in resource.versions
    ]


@router.post("", summary="Request a new resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource", tracer=tracer)
async def create_resource(
    authorization: Authorization,
    current_user: CurrentUser,
    resource_in: ResourceIn,
    db: DBSession,
    background_tasks: BackgroundTasks,
) -> ResourceOut:
    """
    Request a new resources.

    Permission `resource:create` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    resource_in : app.schemas.resource.ResourceIn
        Data about the new resource. HTTP Body.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("resource_in", resource_in.model_dump_json(indent=2))
    await authorization("create")
    if await CRUDResource.get_by_name(db=db, name=resource_in.name) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Resource with name '{resource_in.name}' already exists"
        )
    resource = await CRUDResource.create(db, resource_in=resource_in, maintainer_id=current_user.uid)
    resource_out = ResourceOut.from_db_resource(db_resource=resource)
    background_tasks.add_task(give_permission_to_s3_resource, resource=resource_out)
    background_tasks.add_task(
        give_permission_to_s3_resource_version,
        resource_version=resource_out.versions[0],
        maintainer_id=resource_out.maintainer_id,
    )
    background_tasks.add_task(
        add_s3_resource_version_info,
        s3_resource_version_info=S3ResourceVersionInfo.from_models(
            resource=resource, resource_version=resource.versions[0], maintainer=current_user
        ),
    )
    return resource_out


@router.get("/{rid}", summary="Get a resource")
@start_as_current_span_async("api_get_resource", tracer=tracer)
async def get_resource(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.Status] | SkipJsonSchema[None],
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> ResourceOut:
    """
    Get a specific resource.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    version_status : list[clowmdb.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        "read_any"
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else "read"
    )
    await authorization(rbac_operation)
    return ResourceOut.from_db_resource(
        resource,
        versions=[
            version for version in resource.versions if version_status is None or version.status in version_status
        ],
    )


@router.delete("/{rid}", summary="Delete a resource", status_code=status.HTTP_204_NO_CONTENT)
@start_as_current_span_async("api_delete_resource", tracer=tracer)
async def delete_resource(
    authorization: Authorization,
    resource: CurrentResource,
    db: DBSession,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Delete a resources.

    Permission `resource:delete` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    await authorization("delete")
    await CRUDResource.delete(db, resource_id=resource.resource_id)
    background_tasks.add_task(delete_resource_policy_stmt, resource=ResourceOut.from_db_resource(resource))
    background_tasks.add_task(delete_s3_resource, resource_id=resource.resource_id)
    background_tasks.add_task(delete_cluster_resource, resource_id=resource.resource_id)
