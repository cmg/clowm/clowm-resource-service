from io import BytesIO
from os import urandom
from pathlib import Path
from typing import Annotated, Any, Awaitable, Callable

from clowmdb.models import ResourceVersion
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Query, status
from fastapi.requests import Request
from fastapi.responses import FileResponse, Response
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema
from starlette.background import BackgroundTask

from app.api.background.cluster import (
    delete_cluster_resource_version,
    set_cluster_resource_version_latest,
    synchronize_cluster_resource,
)
from app.api.background.email import (
    send_review_request_email,
    send_review_response_email,
    send_sync_request_email,
    send_sync_response_email,
)
from app.api.background.s3 import (
    add_s3_resource_version_info,
    delete_s3_resource_version,
    get_s3_resource_version_obj,
    give_permission_to_s3_resource_version,
    remove_permission_to_s3_resource_version,
)
from app.api.dependencies import (
    AuthorizationDependency,
    CurrentResource,
    CurrentResourceVersion,
    CurrentUser,
    DBSession,
    S3Resource,
)
from app.core.config import settings
from app.crud import CRUDResourceVersion
from app.s3.s3_resource import get_s3_object
from app.schemas.resource import ResourceOut, S3ResourceVersionInfo
from app.schemas.resource_version import (
    FileTree,
    ResourceVersionIn,
    ResourceVersionOut,
    UserRequestAnswer,
    UserSynchronizationRequestIn,
    resource_version_dir_name,
)
from app.utils import start_as_current_span_async

router = APIRouter(prefix="/resources/{rid}/versions", tags=["ResourceVersion"])
resource_authorization = AuthorizationDependency(resource="resource")
Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(resource_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", summary="List versions of a resource")
@start_as_current_span_async("api_list_resource_versions", tracer=tracer)
async def list_resource_versions(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.Status] | SkipJsonSchema[None],
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> list[ResourceVersionOut]:
    """
    List all the resource version for a specific resource.

    Permission 'resource:read' required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    version_status : list[clowmdb.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        "list_filter"
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else "list"
    )
    await authorization(rbac_operation)
    return [
        ResourceVersionOut.from_db_resource_version(version)
        for version in resource.versions
        if version_status is None or version.status in version_status
    ]


@router.post("", summary="Request new version of a resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource_version", tracer=tracer)
async def request_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version_in: ResourceVersionIn,
    current_user: CurrentUser,
    db: DBSession,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Request a new resource version.

    Permission `resource:update` required if the current user is the maintainer, `resource:update_any` otherwise.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version_in : app.schemas.resource_version.ResourceVersionIn
        Data about the new resource version. HTTP Body.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_in": resource_version_in.model_dump_json(indent=2)}
    )
    rbac_operation = "update"
    if current_user.uid != resource.maintainer_id:
        rbac_operation = "update_any"
    await authorization(rbac_operation)
    resource_version = await CRUDResourceVersion.create(
        db, resource_id=resource.resource_id, release=resource_version_in.release
    )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    background_tasks.add_task(
        give_permission_to_s3_resource_version,
        resource_version=resource_version_out,
        maintainer_id=current_user.uid,
    )
    background_tasks.add_task(
        add_s3_resource_version_info,
        s3_resource_version_info=S3ResourceVersionInfo.from_models(
            resource=resource, resource_version=resource.versions[0], maintainer=current_user
        ),
    )
    return resource_version_out


@router.get("/{rvid}", summary="Get version of a resource")
@start_as_current_span_async("api_get_resource_version", tracer=tracer)
async def get_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    resource_version: CurrentResourceVersion,
) -> ResourceVersionOut:
    """
    Get a specific resource version for a specific resource.

    Permission `resource:read` required. If the status of the resource version is not `LATEST` or `SYNCHRONIZED` and
    the current user is not the maintainer, then the permission `resource:read_any` is required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    rbac_operation = "read"
    # Maintainer can read any version of his workflow, others only synchronized and latest ones
    if current_user.uid != resource.maintainer_id and resource_version.status not in [
        ResourceVersion.Status.SYNCHRONIZED,
        ResourceVersion.Status.LATEST,
    ]:
        rbac_operation = "read_any"
    await authorization(rbac_operation)
    return ResourceVersionOut.from_db_resource_version(resource_version)


@router.get(
    "/{rvid}/tree",
    summary="Download folder structure of resource",
    response_model_exclude_none=True,
    response_model=list[FileTree],
)
@start_as_current_span_async("api_get_resource_version_folder_structure", tracer=tracer)
async def resource_file_tree(
    authorization: Authorization, request: Request, s3: S3Resource, resource_version: CurrentResourceVersion
) -> Response:
    """
    Get the folder structure of the resources. Only available if the resource was previously downloaded to the cluster.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    request : fastapi.requests.Request
        Raw request object to read headers from.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    """
    await authorization("read")
    if not resource_version.status.public:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"can't fetch folder structure of resource version with status {resource_version.status.name}",
        )
    obj = await get_s3_object(
        s3,
        bucket_name=settings.s3.resource_bucket,
        key=resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
        + "/tree.json",
    )
    if obj is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="resource version was synchronized to the cluster yet"
        )
    etag = await obj.e_tag
    # check if browser has the correct object still cached
    if etag == request.headers.get("If-None-Match", None):
        return Response(status_code=status.HTTP_304_NOT_MODIFIED)
    # if obj is larger than 10 MiB, download it to file and stream the file to the client
    if await obj.size > 10485760:
        file = Path(f"/tmp/{urandom(32).hex()}.json")
        with tracer.start_as_current_span(
            "s3_download_obj_to_file",
            attributes={
                "bucket_name": settings.s3.resource_bucket,
                "key": resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
                + "/tree.json",
                "etag": await obj.e_tag,
                "content-length": await obj.size,
                "file_path": str(file),
            },
        ):
            await (await obj.Object()).download_file(str(file))
        # stream file to client and delete it afterwards
        return FileResponse(
            file, headers={"etag": etag}, media_type="application/json", background=BackgroundTask(file.unlink)
        )
    # Download file in memory and send it to the client
    with BytesIO() as f:
        with tracer.start_as_current_span(
            "s3_download_obj_in_memory",
            attributes={
                "bucket_name": settings.s3.resource_bucket,
                "key": resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
                + "/tree.json",
                "etag": await obj.e_tag,
                "content-length": await obj.size,
            },
        ):
            await (await obj.Object()).download_fileobj(f)
        f.seek(0)
        return Response(content=f.read(), headers={"etag": etag}, media_type="application/json")


@router.put("/{rvid}/request_review", summary="Request resource version review")
@start_as_current_span_async("api_request_resource_version_review", tracer=tracer)
async def request_resource_version_review(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    current_user: CurrentUser,
    db: DBSession,
    s3: S3Resource,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Request the review of a resource version.

    Permission `resource:request_review` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    await authorization("request_review")
    if current_user.uid != resource.maintainer_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="only the maintainer can ask for a review")
    if resource_version.status is not ResourceVersion.Status.RESOURCE_REQUESTED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't request sync for resource version with status {resource_version.status.name}",
        )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    if await get_s3_resource_version_obj(ResourceVersionOut.from_db_resource_version(resource_version), s3=s3) is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Missing resource at S3 path {resource_version_out.s3_path}",
        )
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=ResourceVersion.Status.WAIT_FOR_REVIEW
    )
    resource_version_out.status = ResourceVersion.Status.WAIT_FOR_REVIEW
    background_tasks.add_task(
        remove_permission_to_s3_resource_version,
        resource_version=resource_version_out,
    )
    background_tasks.add_task(
        send_review_request_email,
        resource=ResourceOut.from_db_resource(resource),
        version=ResourceVersionOut.from_db_resource_version(resource_version),
    )
    return resource_version_out


@router.put("/{rvid}/request_sync", summary="Request resource version synchronization")
@start_as_current_span_async("api_request_resource_version_sync", tracer=tracer)
async def request_resource_version_sync(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    current_user: CurrentUser,
    db: DBSession,
    background_tasks: BackgroundTasks,
    sync_request: UserSynchronizationRequestIn,
) -> ResourceVersionOut:
    """
    Request the synchronization of a resource version to the cluster.

    Permission `resource:request_sync` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    sync_request : app.schemas.resource_version.UserSynchronizationRequestIn
        Reason for the synchronization request, HTTP Body.
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **sync_request.model_dump(),
        }
    )
    await authorization("request_sync")
    if resource_version.status is not ResourceVersion.Status.APPROVED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't request sync for resource version with status {resource_version.status.name}",
        )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    await CRUDResourceVersion.update_status(
        db,
        resource_version_id=resource_version.resource_version_id,
        status=ResourceVersion.Status.SYNC_REQUESTED,
        values={
            "synchronization_request_description": sync_request.reason,
            "synchronization_request_uid_bytes": current_user.uid_bytes,
        },
    )
    resource_version_out.status = ResourceVersion.Status.SYNC_REQUESTED
    background_tasks.add_task(
        remove_permission_to_s3_resource_version,
        resource_version=resource_version_out,
    )
    background_tasks.add_task(
        send_sync_request_email,
        resource=ResourceOut.from_db_resource(resource),
        version=ResourceVersionOut.from_db_resource_version(resource_version),
        request_reason=sync_request.reason,
        requester=current_user,
    )
    return resource_version_out


@router.put("/{rvid}/review", summary="Review resource version")
@start_as_current_span_async("api_resource_version_review", tracer=tracer)
async def resource_version_review(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    db: DBSession,
    answer: UserRequestAnswer,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Review answer the resource version.

    Permission `resource:review` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    answer : app.schemas.UserRequestAnswer
        Answer to the review request. HTTP Body.
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **answer.model_dump(exclude_none=True),
        }
    )
    await authorization("review")
    if resource_version.status != ResourceVersion.Status.WAIT_FOR_REVIEW:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't deny resource version with status {resource_version.status.name}",
        )

    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    resource_version_out.status = ResourceVersion.Status.DENIED if answer.deny else ResourceVersion.Status.APPROVED
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=resource_version_out.status
    )
    background_tasks.add_task(
        send_review_response_email,
        resource=ResourceOut.from_db_resource(resource),
        version=ResourceVersionOut.from_db_resource_version(resource_version),
        deny_reason=answer.reason if answer.deny else None,
    )
    return resource_version_out


@router.put("/{rvid}/sync", summary="Synchronize resource version with cluster")
@start_as_current_span_async("api_resource_version_sync", tracer=tracer)
async def resource_version_sync(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    db: DBSession,
    s3: S3Resource,
    background_tasks: BackgroundTasks,
    answer: UserRequestAnswer,
) -> ResourceVersionOut:
    """
    Synchronize the resource version to the cluster.

    Permission `resource:sync` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    answer : app.schemas.UserRequestAnswer
        Answer to the synchronizaiton request. HTTP Body.
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **answer.model_dump(exclude_none=True),
        }
    )
    await authorization("sync")
    if resource_version.status not in [
        ResourceVersion.Status.APPROVED,
        ResourceVersion.Status.SYNC_REQUESTED,
        ResourceVersion.Status.SYNC_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't sync resource version with status {resource_version.status.name}",
        )
    elif resource_version.status is not ResourceVersion.Status.SYNC_REQUESTED and answer.deny:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't deny sync request for resource version with status {resource_version.status.name}",
        )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    if (
        not answer.deny
        and await get_s3_resource_version_obj(ResourceVersionOut.from_db_resource_version(resource_version), s3=s3)
        is None
    ):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Missing resource at S3 path {resource_version_out.s3_path}",
        )

    resource_version_out.status = (
        ResourceVersion.Status.APPROVED if answer.deny else ResourceVersion.Status.SYNCHRONIZING
    )
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=resource_version_out.status
    )

    if not answer.deny:
        background_tasks.add_task(
            synchronize_cluster_resource,
            resource=ResourceOut.from_db_resource(resource),
            resource_version=resource_version_out,
        )
    background_tasks.add_task(
        send_sync_response_email,
        resource=resource,
        version=resource_version,
        deny_reason=answer.reason if answer.deny else None,
    )
    return resource_version_out


@router.put("/{rvid}/latest", summary="Set resource version to latest")
@start_as_current_span_async("api_resource_version_set_latest", tracer=tracer)
async def resource_version_latest(
    authorization: Authorization,
    db: DBSession,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Set the resource version as the latest version.

    Permission `resource:set_latest` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    await authorization("set_latest")
    if resource_version.status != ResourceVersion.Status.SYNCHRONIZED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't set resource version to {ResourceVersion.Status.LATEST.name} with status {resource_version.status.name}",
        )
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=ResourceVersion.Status.SETTING_LATEST
    )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    resource_version_out.status = ResourceVersion.Status.SETTING_LATEST
    background_tasks.add_task(set_cluster_resource_version_latest, resource_version=resource_version_out)
    return resource_version_out


@router.delete("/{rvid}/cluster", summary="Delete resource version on cluster")
@start_as_current_span_async("api_resource_version_delete_cluster", tracer=tracer)
async def delete_resource_version_cluster(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    db: DBSession,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Delete the resource version on the cluster.

    Permission `resource:delete_cluster` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    slurm_client : app.slurm.rest_client.SlurmClient
        Slurm client with an open connection. Dependency Injection
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    await authorization("delete_cluster")
    if resource_version.status not in [
        ResourceVersion.Status.SYNCHRONIZED,
        ResourceVersion.Status.LATEST,
        ResourceVersion.Status.CLUSTER_DELETE_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't delete resource version on cluster with status {resource_version.status.name}",
        )
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=ResourceVersion.Status.CLUSTER_DELETING
    )
    resource_version.status = ResourceVersion.Status.CLUSTER_DELETING
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    background_tasks.add_task(delete_cluster_resource_version, resource_version=resource_version_out)
    return resource_version_out


@router.delete("/{rvid}/s3", summary="Delete resource version in S3")
@start_as_current_span_async("api_resource_version_delete_cluster", tracer=tracer)
async def delete_resource_version_s3(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    db: DBSession,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Delete the resource version in the S3 bucket.

    Permission `resource:delete_s3` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    await authorization("delete_s3")
    if resource_version.status not in [
        ResourceVersion.Status.DENIED,
        ResourceVersion.Status.APPROVED,
        ResourceVersion.Status.S3_DELETE_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't delete S3 resource version with status {resource_version.status.name}",
        )
    await CRUDResourceVersion.update_status(
        db, resource_version_id=resource_version.resource_version_id, status=ResourceVersion.Status.S3_DELETING
    )
    resource_version.status = ResourceVersion.Status.S3_DELETING
    background_tasks.add_task(
        delete_s3_resource_version,
        resource_id=resource.resource_id,
        resource_version_id=resource_version.resource_version_id,
    )
    background_tasks.add_task(
        remove_permission_to_s3_resource_version,
        resource_version=ResourceVersionOut.from_db_resource_version(resource_version),
    )

    return ResourceVersionOut.from_db_resource_version(resource_version)
