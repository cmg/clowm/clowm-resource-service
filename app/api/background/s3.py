from io import BytesIO
from typing import TYPE_CHECKING, Any
from uuid import UUID

from botocore.exceptions import ClientError
from clowmdb.models import ResourceVersion
from opentelemetry import trace

from app.api import dependencies
from app.api.background.dependencies import get_background_db
from app.core.config import settings
from app.crud import CRUDResourceVersion
from app.s3.s3_resource import (
    add_s3_bucket_policy_stmt,
    delete_s3_bucket_policy_stmt,
    delete_s3_objects,
    get_s3_object,
    upload_obj,
)
from app.schemas.resource import ResourceOut, S3ResourceVersionInfo
from app.schemas.resource_version import (
    ResourceVersionOut,
    resource_dir_name,
    resource_version_dir_name,
    resource_version_key,
)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import ObjectSummary, S3ServiceResource
else:
    S3ServiceResource = object
    ObjectSummary = object


tracer = trace.get_tracer_provider().get_tracer(__name__)


async def give_permission_to_s3_resource(resource: ResourceOut) -> None:
    """
    Give the maintainer permissions to list the S3 objects of his resource.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        Information about the resource.
    """
    policy: dict[str, Any] = {
        "Sid": str(resource.resource_id),
        "Effect": "Allow",
        "Principal": {"AWS": f"arn:aws:iam:::user/{str(resource.maintainer_id)}"},
        "Resource": f"arn:aws:s3:::{settings.s3.resource_bucket}",
        "Action": ["s3:ListBucket"],
        "Condition": {"StringLike": {"s3:prefix": resource_dir_name(resource.resource_id) + "/*"}},
    }
    async with dependencies.get_background_s3_resource() as s3:
        await add_s3_bucket_policy_stmt(policy, s3=s3, bucket_name=settings.s3.resource_bucket)


async def give_permission_to_s3_resource_version(resource_version: ResourceVersionOut, maintainer_id: UUID) -> None:
    """
    Give the maintainer permissions to upload the resource to the appropriate S3 key.

    Parameters
    ----------
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Information about the resource version.
    maintainer_id : str
        ID of the maintainer
    """
    policy: dict[str, Any] = {
        "Sid": str(resource_version.resource_version_id),
        "Effect": "Allow",
        "Principal": {"AWS": f"arn:aws:iam:::user/{str(maintainer_id)}"},
        "Resource": f"arn:aws:s3:::{resource_version.s3_path[5:]}",
        "Action": ["s3:DeleteObject", "s3:PutObject", "s3:GetObject"],
    }
    async with dependencies.get_background_s3_resource() as s3:
        await add_s3_bucket_policy_stmt(policy, s3=s3, bucket_name=settings.s3.resource_bucket)


async def add_s3_resource_version_info(s3_resource_version_info: S3ResourceVersionInfo) -> None:
    """
    Upload the resource version information to S3 for documentation

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    s3_resource_version_info : app.schemas.resource.S3ResourceVersionInfo
        Resource information object that will be uploaded to S3.
    """
    buf = BytesIO(s3_resource_version_info.model_dump_json(indent=2).encode("utf-8"))
    async with dependencies.get_background_s3_resource() as s3:
        await upload_obj(
            s3=s3,
            bucket_name=settings.s3.resource_bucket,
            key=s3_resource_version_info.s3_path(),
            obj_stream=buf,
            ExtraArgs={"ContentType": "application/json"},
        )


async def remove_permission_to_s3_resource_version(resource_version: ResourceVersionOut) -> None:
    """
    Remove the permission of the maintainer to upload a resource to S3.

    Parameters
    ----------
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Information about the resource version.
    """
    async with dependencies.get_background_s3_resource() as s3:
        await delete_s3_bucket_policy_stmt(
            s3, bucket_name=settings.s3.resource_bucket, sid=str(resource_version.resource_version_id)
        )


async def delete_s3_resource(resource_id: UUID) -> None:
    """
    Delete all objects related to a resource in S3.

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource
    """
    async with dependencies.get_background_s3_resource() as s3:
        await delete_s3_objects(
            s3, bucket_name=settings.s3.resource_bucket, prefix=resource_dir_name(resource_id) + "/"
        )


async def delete_s3_resource_version(resource_id: UUID, resource_version_id: UUID) -> None:
    """
    Delete all objects related to a resource version in S3.

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource
    resource_version_id : uuid.UUID
        ID of the resource version
    """
    try:
        async with dependencies.get_background_s3_resource() as s3:
            await delete_s3_objects(
                s3,
                bucket_name=settings.s3.resource_bucket,
                prefix=resource_version_dir_name(resource_id, resource_version_id),
            )
        async with get_background_db() as db:
            await CRUDResourceVersion.update_status(
                db, status=ResourceVersion.Status.S3_DELETED, resource_version_id=resource_version_id
            )
    except ClientError:
        async with get_background_db() as db:
            await CRUDResourceVersion.update_status(
                db, status=ResourceVersion.Status.S3_DELETE_ERROR, resource_version_id=resource_version_id
            )


async def get_s3_resource_version_obj(
    resource_version: ResourceVersionOut,
    s3: S3ServiceResource | None = None,
) -> ObjectSummary | None:
    """
    Delete all objects related to a resource version in S3.

    Parameters
    ----------
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Information about the resource version.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource | None
        S3 Service to perform operations on buckets. If None, a new connection will be established.

    Returns
    -------
    obj : types_aiobotocore_s3.service_resource.ObjectSummary | None
        The object summary of the resource version if it exists.
    """

    async def func(s3_inner: S3ServiceResource) -> ObjectSummary | None:
        return await get_s3_object(
            s3=s3_inner,
            bucket_name=settings.s3.resource_bucket,
            key=resource_version_key(
                resource_id=resource_version.resource_id, resource_version_id=resource_version.resource_version_id
            ),
        )

    if s3 is None:
        async with dependencies.get_background_s3_resource() as s3_outer:
            return await func(s3_outer)
    return await func(s3)  # type: ignore[arg-type]


async def delete_resource_policy_stmt(resource: ResourceOut) -> None:
    """
    Delete all bucket policy statements regarding ths resource.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        Information about the resource.
    """
    async with dependencies.get_background_s3_resource() as s3:
        await delete_s3_bucket_policy_stmt(
            s3=s3,
            bucket_name=settings.s3.resource_bucket,
            sid=[str(resource.resource_id)]
            + [str(resource_version.resource_version_id) for resource_version in resource.versions],
        )
