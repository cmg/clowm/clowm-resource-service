from asyncio import sleep as async_sleep
from enum import StrEnum, unique
from pathlib import Path
from typing import Any
from uuid import UUID

from clowmdb.models import ResourceVersion
from httpx import HTTPError
from mako.lookup import TemplateLookup
from opentelemetry import trace
from sqlalchemy.sql import text

from app.api.background.dependencies import get_async_slurm_client, get_background_db
from app.api.background.email import send_sync_error_email, send_sync_success_email
from app.core.config import settings
from app.crud import CRUDResourceVersion, CRUDUser
from app.schemas.resource import ResourceOut
from app.schemas.resource_version import ResourceVersionOut, resource_dir_name, resource_version_dir_name
from app.slurm import SlurmClient, SlurmJobSubmission
from app.utils import AsyncJob, Job
from app.utils.backoff_strategy import ExponentialBackoff

tracer = trace.get_tracer_provider().get_tracer(__name__)

_scriptsTemplateLookup = TemplateLookup(directories=["app/templates/scripts"], module_directory="/tmp/mako_modules")


@unique
class ResourceScriptsTemplates(StrEnum):
    SYNCHRONIZE = "synchronize_resource_version"
    SET_LATEST = "set_latest_resource_version"
    DELETE = "delete_resource"
    DELETE_VERSION = "delete_resource_version"

    def render(self, **kwargs: Any) -> str:
        """
        Render a resource script templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the email template

        Returns
        -------
        script : str
            The rendered script
        """
        return _scriptsTemplateLookup.get_template(f"{self}.sh.tmpl").render(**kwargs)


async def update_db_resource_wrapper(**kwargs: Any) -> None:
    async with get_background_db() as db:
        await CRUDResourceVersion.update_status(db=db, **kwargs)


async def synchronize_cluster_resource(resource: ResourceOut, resource_version: ResourceVersionOut) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        Resource schema of the corresponding version.
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Resource version schema to synchronize.
    """
    synchronization_script = ResourceScriptsTemplates.SYNCHRONIZE.render(
        s3_url=settings.s3.uri,
        resource_version_s3_folder=f"{settings.s3.resource_bucket}/{resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)}",
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
        ),
    )

    async def failed_job(error_msg: str | None = None) -> None:
        await update_db_resource_wrapper(
            resource_version_id=resource_version.resource_version_id,
            status=ResourceVersion.Status.SYNC_ERROR,
            resource_id=None,
        )
        await send_sync_error_email(resource=resource, version=resource_version, error_msg=error_msg)

    try:
        job_submission = SlurmJobSubmission(
            script=synchronization_script.strip(),
            job={
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Synchronize {str(resource_version.resource_version_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory)
                    / f"slurm-synchronize-{resource_version.resource_version_id.hex}.out"
                ),
                "environment": {
                    "AWS_ACCESS_KEY_ID": settings.s3.access_key,
                    "AWS_SECRET_ACCESS_KEY": settings.s3.secret_key.get_secret_value(),
                },
            },
        )
        async with get_async_slurm_client() as slurm_client:
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job_submission=job_submission)

            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.Status.SYNCHRONIZING,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )

            async def success_job() -> None:
                user = None
                async with get_background_db() as db:
                    version = await CRUDResourceVersion.get(
                        db=db,
                        resource_version_id=resource_version.resource_version_id,
                        resource_id=resource_version.resource_id,
                    )
                    if version is not None and version.synchronization_request_uid is not None:
                        user = await CRUDUser.get(db=db, uid=version.synchronization_request_uid)
                    await CRUDResourceVersion.update_status(
                        db=db,
                        resource_version_id=resource_version.resource_version_id,
                        status=ResourceVersion.Status.SYNCHRONIZED,
                        resource_id=resource_version.resource_id,
                        values={"synchronization_request_uid_bytes": None},
                    )

                if user is not None:
                    await send_sync_success_email(resource=resource, version=resource_version, requester=user)

            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(success_job),
                failed_job=AsyncJob(failed_job),
            )
    except (HTTPError, KeyError) as e:  # pragma: no cover
        await failed_job(error_msg=str(e))


async def delete_cluster_resource(
    resource_id: UUID,
) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource to delete
    """

    delete_script = ResourceScriptsTemplates.DELETE.render(
        resource_path=str(Path(settings.cluster.resource_cluster_path) / resource_dir_name(resource_id))
    )

    try:
        job_submission = SlurmJobSubmission(
            script=delete_script.strip(),
            job={
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Delete {str(resource_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory) / f"slurm-delete-{resource_id.hex}.out"
                ),
                "environment": {"NONEMPTY": "NONEMPTY"},
            },
        )
        async with get_async_slurm_client() as slurm_client:
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job_submission=job_submission)
            await _monitor_proper_job_execution(slurm_client=slurm_client, slurm_job_id=slurm_job_id)
    except (HTTPError, KeyError):  # pragma: no cover
        pass


async def set_cluster_resource_version_latest(resource_version: ResourceVersionOut) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Resource version schema to synchronize.
    """
    set_latest_script = ResourceScriptsTemplates.SET_LATEST.render(
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(
                resource_id=resource_version.resource_id,
                resource_version_id=resource_version.resource_version_id,
            )
        ),
        latest_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_dir_name(
                resource_id=resource_version.resource_id,
            )
            / "latest"
        ),
    )
    failed_job = AsyncJob(
        update_db_resource_wrapper,
        status=ResourceVersion.Status.SYNCHRONIZED,
        resource_version_id=resource_version.resource_version_id,
        resource_id=resource_version.resource_id,
    )
    job_submission = SlurmJobSubmission(
        script=set_latest_script.strip(),
        job={
            "current_working_directory": settings.cluster.working_directory,
            "name": f"SET Latest {str(resource_version.resource_version_id)}",
            "requeue": False,
            "standard_output": str(
                Path(settings.cluster.working_directory)
                / f"slurm-set-latest-{resource_version.resource_version_id.hex}.out"
            ),
            "environment": {"NONEMPTY": "NONEMPTY"},
        },
    )
    try:
        async with get_async_slurm_client() as slurm_client:
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job_submission=job_submission)
            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.Status.SETTING_LATEST,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )
            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.Status.LATEST,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=resource_version.resource_id,
                ),
                failed_job=failed_job,
            )
    except (HTTPError, KeyError) as e:  # pragma: no cover
        await failed_job()
        raise e


async def delete_cluster_resource_version(
    resource_version: ResourceVersionOut,
) -> None:
    """
    Delete a resource version on the cluster

    Parameters
    ----------
    resource_version : app.schemas.resource_version.ResourceVersionOut
        Resource version schema to delete.
    """
    delete_script = ResourceScriptsTemplates.DELETE_VERSION.render(
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(
                resource_id=resource_version.resource_id,
                resource_version_id=resource_version.resource_version_id,
            )
        ),
        latest_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_dir_name(
                resource_id=resource_version.resource_id,
            )
            / "latest"
        ),
    )

    try:
        job_submission = SlurmJobSubmission(
            script=delete_script.strip(),
            job={
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Delete {str(resource_version.resource_version_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory)
                    / f"slurm-delete-version-{resource_version.resource_version_id.hex}.out"
                ),
                "environment": {"NONEMPTY": "NONEMPTY"},
            },
        )
        async with get_async_slurm_client() as slurm_client:
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job_submission=job_submission)
            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.Status.CLUSTER_DELETING,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )
            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.Status.APPROVED,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=None,
                    values={"cluster_deleted_timestamp": text("UNIX_TIMESTAMP()")},
                ),
                failed_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.Status.CLUSTER_DELETE_ERROR,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=resource_version.resource_id,
                ),
            )
    except (HTTPError, KeyError):  # pragma: no cover
        pass


async def _monitor_proper_job_execution(
    slurm_client: SlurmClient, slurm_job_id: int, success_job: Job | None = None, failed_job: Job | None = None
) -> None:
    """
    Check in an interval based on a backoff strategy if the slurm job is still running
    the workflow execution in the database is not marked as finished.

    Parameters
    ----------
    slurm_client : app.slurm.rest_client.SlurmClient
        Slurm Rest Client to communicate with Slurm cluster.
    slurm_job_id : int
        ID of the slurm job to monitor
    success_job : app.utils.Job | None
        Function to execute after the slurm job was successful
    failed_job : app.utils.Job | None
        Function to execute after the slurm job was unsuccessful
    """
    # exponential backoff to 5 minutes
    sleep_generator = ExponentialBackoff(max_value=300)

    # Closure around monitor_job code
    async def monitor_job() -> None:
        with tracer.start_span("monitor_job") as span:
            span.set_attributes({"slurm_job_id": slurm_job_id})
            job_state = await slurm_client.job_state(slurm_job_id)
            if job_state != SlurmClient.JobState.RUNNING:
                if job_state == SlurmClient.JobState.SUCCESS and success_job is not None:
                    if success_job.is_async:
                        await success_job()
                    else:  # pragma: no cover
                        success_job()
                elif job_state == SlurmClient.JobState.ERROR and failed_job is not None:
                    if failed_job.is_async:
                        await failed_job()
                    else:  # pragma: no cover
                        failed_job()
                sleep_generator.close()

    await monitor_job()
    for sleep_time in sleep_generator:  # pragma: no cover
        await async_sleep(sleep_time)
        await monitor_job()
