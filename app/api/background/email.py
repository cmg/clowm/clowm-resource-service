from email import utils as email_utils
from email.message import EmailMessage
from enum import StrEnum, unique
from typing import Any

from clowmdb.models import Resource, ResourceVersion, User
from mako.lookup import TemplateLookup
from opentelemetry import trace

from app.api import dependencies
from app.api.background.dependencies import get_background_db, smtp_connection
from app.core.config import settings
from app.core.security import request_role_users
from app.crud import CRUDUser
from app.schemas.resource import ResourceOut
from app.schemas.resource_version import ResourceVersionOut
from app.schemas.security import Roles

tracer = trace.get_tracer_provider().get_tracer(__name__)

_emailHtmlLookup = TemplateLookup(directories=["app/templates/email/html"], module_directory="/tmp/mako_modules")
_emailPlainLookup = TemplateLookup(directories=["app/templates/email/plain"], module_directory="/tmp/mako_modules")


@unique
class EmailTemplates(StrEnum):
    REVIEW_REQUEST = "review_request"
    REVIEW_RESPONSE = "review_response"
    SYNC_REQUEST = "sync_request"
    SYNC_RESPONSE = "sync_response"
    SYNC_SUCCESS = "sync_success"
    SYNC_ERROR = "sync_error"

    def render(self, **kwargs: Any) -> tuple[str, str]:
        """
        Render both HTML and plain text email templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the email template

        Returns
        -------
        html, plain : tuple[str, str]
            The tuple of the rendered HTML and plain text email
        """
        return _emailHtmlLookup.get_template(f"{self}.html.tmpl").render(
            settings=settings, **kwargs
        ), _emailPlainLookup.get_template(f"{self}.txt.tmpl").render(settings=settings, **kwargs)


def _format_user_to_email(user: User) -> str:
    return f"{user.display_name} <{user.email}>"


def send_email(
    recipients: list[User] | User,
    subject: str,
    plain_msg: str,
    html_msg: str | None = None,
) -> None:
    """
    Send an email.

    Parameters
    ----------
    recipients: list[clowmdb.models.User] | clowmdb.models.User
        The recipients of the email. Multiple will be sent via BCC
    subject: str
        The subject of the email.
    plain_msg: str
        An alternative message in plain text format.
    html_msg : str
        An alternative message in HTML format.
    """
    email_msg = EmailMessage()
    email_msg["Subject"] = subject
    email_msg["From"] = (
        f"CloWM <{settings.smtp.sender_email.email}>"
        if settings.smtp.sender_email.email.startswith(settings.smtp.sender_email.name)
        else str(settings.smtp.sender_email)
    )
    if isinstance(recipients, list):
        email_msg["Bcc"] = ",".join(_format_user_to_email(user) for user in recipients)
    else:
        email_msg["To"] = _format_user_to_email(recipients)
    email_msg["Date"] = email_utils.formatdate()
    if settings.smtp.reply_email is not None:  # pragma: no cover
        email_msg["Reply-To"] = (
            f"CloWM Support <{settings.smtp.reply_email.email}>"
            if settings.smtp.reply_email.email.startswith(settings.smtp.reply_email.name)
            else str(settings.smtp.reply_email)
        )
    email_msg.set_content(plain_msg)
    if html_msg is not None:
        email_msg.add_alternative(html_msg, subtype="html")
    with tracer.start_as_current_span(
        "background_send_email",
        attributes={
            "subject": subject,
            "recipients": (
                [str(user.uid) for user in recipients] if isinstance(recipients, list) else str(recipients.uid)
            ),
        },
    ):
        with smtp_connection() as server:
            server.send_message(email_msg)


async def send_review_request_email(resource: ResourceOut, version: ResourceVersionOut) -> None:
    """
    Email all administrators and reviewers that a new resource version was registered and needs to be reviewed.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        The reviewed workflow.
    version : app.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    """
    async with dependencies.get_background_http_client() as client:
        lids = (await request_role_users(client=client, roles=[Roles.ADMINISTRATOR, Roles.REVIEWER])).result.roles
    async with get_background_db() as db:
        maintainer = await CRUDUser.get(db=db, uid=resource.maintainer_id)
        recipients = await CRUDUser.get_by_lifescience_id(
            db=db, lifescience_ids=[lid for users in lids.values() for lid in users]
        )
    html, plain = EmailTemplates.REVIEW_REQUEST.render(resource=resource, version=version, maintainer=maintainer)
    send_email(
        recipients=recipients,
        subject=f"Resource version review {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_review_response_email(
    resource: ResourceOut, version: ResourceVersionOut, deny_reason: str | None
) -> None:
    """
    Email the maintainer of a resource that the version was reviewed.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        The reviewed workflow.
    version : app.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    deny_reason : str | None
        If the version was denied, a reason must be given
    """
    async with get_background_db() as db:
        maintainer = await CRUDUser.get(db=db, uid=resource.maintainer_id)
    html, plain = EmailTemplates.REVIEW_RESPONSE.render(
        resource=resource, version=version, maintainer=maintainer, deny_reason=deny_reason
    )
    send_email(
        recipients=maintainer,
        subject=f"Resource version review response {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_request_email(
    resource: ResourceOut, version: ResourceVersionOut, request_reason: str, requester: User
) -> None:
    """
    Email all administrators and reviewers that a new resource was registered and needs to be reviewed.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        The reviewed workflow.
    version : app.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    request_reason : str
        The reason for the synchronization request
    requester : clowmdb.models.User
        The user who requested this synchronization
    """
    async with dependencies.get_background_http_client() as client:
        lids = (await request_role_users(client=client, roles=[Roles.ADMINISTRATOR])).result.roles
    async with get_background_db() as db:
        recipients = await CRUDUser.get_by_lifescience_id(
            db=db, lifescience_ids=[lid for users in lids.values() for lid in users]
        )
    html, plain = EmailTemplates.SYNC_REQUEST.render(
        resource=resource, version=version, requester=requester, request_reason=request_reason
    )
    send_email(
        recipients=recipients,
        subject=f"Resource sync request {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_response_email(resource: Resource, version: ResourceVersion, deny_reason: str | None) -> None:
    """
    Email the sync requester the response to the sync request.

    Parameters
    ----------
    resource : clowmdb.models.Resource
        The reviewed workflow.
    version : clowmdb.models.ResourceVersion
        The reviewed workflow version.
    deny_reason : str | None
        If the request was denied, a reason must be given
    """
    if version.synchronization_request_uid is None:
        return
    async with get_background_db() as db:
        requester = await CRUDUser.get(db=db, uid=version.synchronization_request_uid)
    html, plain = EmailTemplates.SYNC_RESPONSE.render(
        resource=resource, version=version, requester=requester, deny_reason=deny_reason
    )
    send_email(
        recipients=requester,
        subject=f"Resource sync request response {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_success_email(resource: ResourceOut, version: ResourceVersionOut, requester: User) -> None:
    """
    Email the sync requester when the synchronization process is done..

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        The reviewed workflow.
    version : app.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    """
    html, plain = EmailTemplates.SYNC_SUCCESS.render(resource=resource, version=version, requester=requester)
    send_email(
        recipients=requester,
        subject=f"Resource sync success {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_error_email(
    resource: ResourceOut, version: ResourceVersionOut, error_msg: str | None = None
) -> None:
    """
    Email all administrators that a resource synchronization has failed.

    Parameters
    ----------
    resource : app.schemas.resource.ResourceOut
        The reviewed workflow.
    version : app.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    error_msg : str | None, default None
        An optional error message
    """
    async with dependencies.get_background_http_client() as client:
        lids = (await request_role_users(client=client, roles=[Roles.ADMINISTRATOR])).result.roles
    async with get_background_db() as db:
        recipients = await CRUDUser.get_by_lifescience_id(
            db=db, lifescience_ids=[lid for users in lids.values() for lid in users]
        )
    html, plain = EmailTemplates.SYNC_ERROR.render(resource=resource, version=version, error_msg=error_msg)
    send_email(
        recipients=recipients,
        subject=f"Resource sync error {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )
