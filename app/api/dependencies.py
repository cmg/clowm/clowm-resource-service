from contextlib import asynccontextmanager
from typing import TYPE_CHECKING, Annotated, AsyncIterator, Awaitable, Callable
from uuid import UUID

from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from clowmdb.db.session import get_async_session
from clowmdb.models import Resource, ResourceVersion, User
from fastapi import Depends, HTTPException, Path, Request, status
from fastapi.security import HTTPBearer
from fastapi.security.http import HTTPAuthorizationCredentials
from httpx import AsyncClient
from opentelemetry import trace
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.core.security import decode_token, request_authorization
from app.crud import CRUDResource, CRUDUser
from app.s3.s3_resource import boto_session
from app.schemas.security import JWT, AuthzRequest, AuthzResponse
from app.slurm.rest_client import SlurmClient as _SlurmClient
from app.utils.otlp import start_as_current_span_async

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

bearer_token = HTTPBearer(description="JWT Header", auto_error=False)

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def get_s3_resource(request: Request) -> S3ServiceResource:  # pragma: no cover
    """
    Get an async S3 service with an open connection.

    Parameters
    ----------
    request : fastapi.Request
        Request object from FastAPI where the client is attached to.

    Returns
    -------
    client : types_aiobotocore_s3.service_resource.S3ServiceResource
        Async S3 resource with open connection
    """
    return request.app.s3_resource


S3Resource = Annotated[S3ServiceResource, Depends(get_s3_resource)]


@asynccontextmanager
async def get_background_s3_resource() -> AsyncIterator[S3ServiceResource]:  # pragma: no cover
    async with boto_session.resource(
        service_name="s3",
        endpoint_url=str(settings.s3.uri)[:-1],
        verify=settings.s3.uri.scheme == "https",
    ) as s3_resource:
        yield s3_resource


async def get_db() -> AsyncIterator[AsyncSession]:  # pragma: no cover
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Returns
    -------
    db : AsyncIterator[AsyncSession, None]
        Async session object with the database
    """
    async with get_async_session(str(settings.db.dsn_async), verbose=settings.db.verbose) as db:
        yield db


DBSession = Annotated[AsyncSession, Depends(get_db)]


async def get_httpx_client(request: Request) -> AsyncClient:  # pragma: no cover
    """
    Get a async http client with an open connection.

    Parameters
    ----------
    request : fastapi.Request
        Request object from FastAPI where the client is attached to.

    Returns
    -------
    client : httpx.AsyncClient
        Async http client with open connection
    """
    return request.app.requests_client


HTTPClient = Annotated[AsyncClient, Depends(get_httpx_client)]


@asynccontextmanager
async def get_background_http_client() -> AsyncIterator[AsyncClient]:  # pragma: no cover
    async with AsyncClient() as client:
        yield client


def get_slurm_client(client: HTTPClient) -> _SlurmClient:
    """
    Get a async slurm client.

    Parameters
    ----------
    client : httpx.AsyncClient
        Async http client toi inject into the slurm client

    Returns
    -------
    slurm_client : app.slurm.rest_client.SlurmClient
        Slurm client with an open connection.
    """
    return SlurmClient(client=client)


SlurmClient = Annotated[_SlurmClient, Depends(get_slurm_client)]


def get_decode_jwt_function() -> Callable[[str], dict[str, str]]:  # pragma: no cover
    """
    Get function to decode and verify the JWT.

    This will be injected into the function which will handle the JWT. With this approach, the function to decode and
    verify the JWT can be overwritten during tests.

    Returns
    -------
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    """
    return decode_token


@start_as_current_span_async("decode_jwt", tracer=tracer)
async def decode_bearer_token(
    token: Annotated[HTTPAuthorizationCredentials | None, Depends(bearer_token)],
    decode: Annotated[Callable[[str], dict[str, str]], Depends(get_decode_jwt_function)],
    db: DBSession,
) -> JWT:
    """
    Get the decoded JWT or reject request if it is not valid or the user doesn't exist.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : fastapi.security.http.HTTPAuthorizationCredentials
        Bearer token sent with the HTTP request. Dependency Injection.
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    try:
        jwt = JWT(**decode(token.credentials), raw_token=token.credentials)
        trace.get_current_span().set_attributes({"exp": jwt.exp.isoformat(), "uid": jwt.sub})
        await get_current_user(jwt, db)  # make sure the user exists
        return jwt
    except ExpiredTokenError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT Signature has expired")
    except (DecodeError, BadSignatureError):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")


async def get_current_user(token: Annotated[JWT, Depends(decode_bearer_token)], db: DBSession) -> User:
    """
    Get the current user from the database based on the JWT.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User associated with the JWT sent with the HTTP request.
    """
    try:
        uid = UUID(token.sub)
    except ValueError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")
    user = await CRUDUser.get(db, uid)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


CurrentUser = Annotated[User, Depends(get_current_user)]


class AuthorizationDependency:
    """
    Class to parameterize the authorization request with the resource to perform an operation on.
    """

    def __init__(self, resource: str):
        """
        Parameters
        ----------
        resource : str
            Resource parameter for the authorization requests
        """
        self.resource = resource

    def __call__(
        self,
        user: CurrentUser,
        client: HTTPClient,
    ) -> Callable[[str], Awaitable[AuthzResponse]]:
        """
        Get the function to request the authorization service with the resource, JWT and HTTP Client already injected.

        Parameters
        ----------
        user : clowmdb.models.User
            The current user based on the JWT. Dependency Injection.
        client : httpx.AsyncClient
            HTTP Client with an open connection. Dependency Injection.

        Returns
        -------
        authorization_function : Callable[[str], Awaitable[app.schemas.security.AuthzResponse]]
            Async function which ask the Auth service for authorization. It takes the operation as the only input.
        """

        async def authorization_wrapper(operation: str) -> AuthzResponse:
            params = AuthzRequest(operation=operation, resource=self.resource, uid=user.lifescience_id)
            return await request_authorization(request_params=params, client=client)

        return authorization_wrapper


async def get_current_resource(rid: Annotated[UUID, Path()], db: DBSession) -> Resource:
    """
    Get the current resource from the database based on the ID in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    rid : uuid. UUID
        ID of a resource. Path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path.
    """
    resource = await CRUDResource.get(db, resource_id=rid)
    if resource:
        return resource
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource not found")


CurrentResource = Annotated[Resource, Depends(get_current_resource)]


async def get_current_resource_version(rvid: Annotated[UUID, Path()], resource: CurrentResource) -> ResourceVersion:
    """
    Get the current resource version from the database based on the ID in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    rvid : uuid. UUID
        ID of a resource version. Path parameter.
    resource : clowmdb.models.Resource
        Resource associated with the ID in the path.

    Returns
    -------
    resource_version : clowmdb.models.ResourceVersion
        Resource Version associated with the ID in the path.
    """
    resource_version: list[ResourceVersion] = [
        version for version in resource.versions if version.resource_version_id == rvid
    ]
    if len(resource_version) > 0:
        return resource_version[0]
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource version not found")


CurrentResourceVersion = Annotated[ResourceVersion, Depends(get_current_resource_version)]
