import logging

import httpx
from fastapi import status
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 3  # 3 minutes
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    try:
        response = httpx.get(str(settings.s3.uri), timeout=5.0, follow_redirects=False)
        assert response.status_code == status.HTTP_200_OK
    except Exception as e:
        logger.error(e)
        raise e


def main() -> None:
    logger.info("Check S3 connection")
    init()
    logger.info("S3 connection established")


if __name__ == "__main__":
    main()
