import json
from io import BytesIO
from typing import TYPE_CHECKING, Any

import aioboto3
from botocore.exceptions import ClientError
from opentelemetry import trace

from app.core.config import settings

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import BucketPolicy, ObjectSummary, S3ServiceResource
else:
    S3ServiceResource = object
    BucketPolicy = object
    ObjectSummary = object


tracer = trace.get_tracer_provider().get_tracer(__name__)

boto_session = aioboto3.Session(
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
)


async def load_s3_bucket_policy(s3_policy: BucketPolicy) -> dict[str, Any]:
    """
    Loads hte bucket policy form the BucketPolicy object. If there is no policy, an empty one will be generated.

    Parameters
    ----------
    s3_policy : types_aiobotocore_s3.service_resource.BucketPolicy
        BucketPolivy object to load the policy from.

    Returns
    -------
    policy : dict[str, Any]
        The serialized bucket policy.
    """
    with tracer.start_as_current_span("s3_load_bucket_policy", attributes={"bucket_name": s3_policy.bucket_name}):
        try:
            return json.loads(await s3_policy.policy)
        except ClientError:  # pragma: no cover
            return {"Version": "2012-10-17", "Statement": []}


async def add_s3_bucket_policy_stmt(*stmts: dict[str, Any], s3: S3ServiceResource, bucket_name: str) -> None:
    """
    Add a Statement to a bucket policy. If it doesn't exist, then create a policy.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    stmts : dict[str, Any]
        Statements to add to the policy.
    """
    with tracer.start_as_current_span(
        "s3_add_bucket_policy_statement", attributes={"bucket_name": bucket_name, "policy_stmt": json.dumps(stmts)}
    ):
        s3_policy = await s3.BucketPolicy(bucket_name=bucket_name)
        policy = await load_s3_bucket_policy(s3_policy)
        for stmt in stmts:
            policy["Statement"].append(stmt)
        await s3_policy.put(Policy=json.dumps(policy))


async def delete_s3_bucket_policy_stmt(s3: S3ServiceResource, bucket_name: str, sid: str | list[str]) -> None:
    """
    Delete one or multiple Statement based on the Sid from a bucket policy.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    sid : str | list[str]
        ID or IDs of the statement(s).
    """
    with tracer.start_as_current_span(
        "s3_delete_bucket_policy_statement", attributes={"bucket_name": bucket_name, "sid": sid}
    ):
        s3_policy = await s3.BucketPolicy(bucket_name=bucket_name)
        policy = await load_s3_bucket_policy(s3_policy)

        if isinstance(sid, list):
            policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] not in sid]
        else:
            policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] != sid]
        await s3_policy.put(Policy=json.dumps(policy))


async def get_s3_object(s3: S3ServiceResource, bucket_name: str, key: str) -> ObjectSummary | None:
    """
    Get the object summary from S3 if the object exists.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    key : str
        Key of the objects.

    Returns
    -------
    obj : types_aiobotocore_s3.service_resource.ObjectSummary | None
        The object summary if the object exists.
    """
    with tracer.start_as_current_span(
        "s3_get_object_meta_data", attributes={"bucket_name": bucket_name, "key": key}
    ) as span:
        try:
            obj = await s3.ObjectSummary(bucket_name=bucket_name, key=key)
            await obj.load()
        except ClientError:
            return None
        span.set_attributes(
            {"size": await obj.size, "last_modified": (await obj.last_modified).isoformat(), "etag": await obj.e_tag}
        )
        return obj


async def delete_s3_objects(s3: S3ServiceResource, bucket_name: str, prefix: str) -> None:
    """
    Delete multiple S3 objects based on a prefix.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    prefix : str
        Prefix of the keys that should get deleted.
    """
    with tracer.start_as_current_span(
        "s3_delete_objects", attributes={"bucket_name": bucket_name, "prefix": prefix}
    ) as span:
        bucket = await s3.Bucket(bucket_name)
        deleted_objs = await bucket.objects.filter(Prefix=prefix).delete()
        if len(deleted_objs) > 0:
            deleted_keys = [obj["Key"] for obj in deleted_objs[0]["Deleted"]]
            span.set_attribute("keys", deleted_keys)


async def upload_obj(s3: S3ServiceResource, obj_stream: BytesIO, bucket_name: str, key: str, **kwargs: Any) -> None:
    """
    Upload a object in a buffer to
    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    obj_stream : io.BytesIO
        Stream from which the object content is read and uploaded.
    bucket_name : str
        Name of the bucket.
    key : str
        Key of the objects.
    kwargs : Any
        Additional arguments to the upload_fileobj function

    """
    with tracer.start_as_current_span("s3_upload_object", attributes={"bucket_name": bucket_name, "key": key}):
        bucket = await s3.Bucket(bucket_name)
        with obj_stream as f:
            await bucket.upload_fileobj(f, Key=key, **kwargs)
