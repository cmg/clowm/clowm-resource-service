import json
from uuid import uuid4

from fastapi import status
from httpx import Request, Response

from app.schemas.security import AuthzRequest, AuthzResponse, RoleUsersResponse
from app.tests.mocks import MockHTTPService

_json_header = {"content-type": "application/json"}


class MockOpaService(MockHTTPService):
    """
    Class to mock the Open Policy Agent service.
    Has a simplified role management. A user can be either "Admin" or "Normal User".
    """

    def __init__(self) -> None:
        super().__init__()
        self._users: dict[str, bool] = {}

    def add_user(self, uid: str, privileged: bool = False) -> None:
        """
        Add a user to the mock service.

        Parameters
        ----------
        uid : str
            ID of a user.
        privileged : bool, default False
            Flag if the user is an Admin or not.
        """
        self._users[uid] = privileged

    def delete_user(self, uid: str) -> None:
        """
        Delete a user in the mock service.

        Parameters
        ----------
        uid : str
            ID of the user to delete.
        """
        if uid in self._users.keys():
            del self._users[uid]

    def reset(self) -> None:
        """
        Reset the mock service to its initial state.
        """
        super().reset()
        self._users = {}

    def handle_request(self, request: Request, **kwargs: bool) -> Response:
        """
        Handle the raw request that is sent to the mock service.

        Parameters
        ----------
        request: httpx.Request
            Raw HTTP request object.

        Returns
        -------
        response : httpx.Response
            Appropriate response to the received request.
        """
        if request.url.path == "/v1/data/clowm/authz/allow":
            authz_request = AuthzRequest.model_validate(json.loads(request.read().decode("utf-8"))["input"])
            if self.send_error or authz_request.uid not in self._users:
                result = False
            else:
                result = not MockOpaService.request_admin_permission(authz_request) or self._users[authz_request.uid]
            return Response(
                status_code=status.HTTP_200_OK,
                headers=_json_header,
                content=AuthzResponse(result=result, decision_id=uuid4()).model_dump_json(),
            )
        return Response(
            status_code=status.HTTP_200_OK,
            headers=_json_header,
            content=RoleUsersResponse.model_validate(
                {"decision_id": str(uuid4()), "result": {"roles": {"user": list(self._users.keys())}}}
            ).model_dump_json(),
        )

    @staticmethod
    def request_admin_permission(authz_request: AuthzRequest) -> bool:
        """
        Helper function to determine if the authorization request needs the 'administrator' role.

        Parameters
        ----------
        authz_request : app.schemas.security.AuthzRequest
            Body of the request.

        Returns
        -------
        decision : bool
            Flag if the request needs the 'administrator' role
        """
        checks = "any" in authz_request.operation
        if "bucket_permission" in authz_request.resource:
            checks = checks or "all" in authz_request.operation
        return checks
