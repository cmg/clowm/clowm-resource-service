import json
import re
from typing import Any
from uuid import UUID

from fastapi import status
from httpx import Headers, Request, Response

from app.tests.mocks import MockHTTPService

SlurmRequestBody = dict[str, Any]


class MockSlurmCluster(MockHTTPService):
    """
    Class to mock the Rest API of a Slurm cluster
    """

    _method_not_allowed_response = Response(
        status_code=status.HTTP_405_METHOD_NOT_ALLOWED, text="Requested REST method is not defined at URL."
    )

    def __init__(self) -> None:
        super().__init__()
        self._request_bodies: list[SlurmRequestBody] = []
        self._job_states: list[bool] = []
        self.base_path = "slurm/v0.0.38"
        self._job_path_regex = re.compile(r"^/slurm/v0.0.38/job/[\d]*$")

    def handle_request(self, request: Request, **kwargs: bool) -> Response:
        """
        Handle the raw request that is sent to the API.

        Parameters
        ----------
        request: httpx.Request
            Raw HTTP request object.

        Returns
        -------
        response : httpx.Response
            Appropriate response to the request
        """
        if self.send_error:
            return Response(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # Authorize request
        error_response = MockSlurmCluster.authorize_request(request.headers)
        if error_response is not None:
            return error_response
        # If a job should be submitted
        if request.url.path == f"/{self.base_path}/job/submit":
            # Route supports POST Method
            if request.method == "POST":
                # Parse request and save it
                job_id = self.add_workflow_execution(json.loads(request.read().decode("utf-8")))
                # Return index into list with saved requests
                return Response(status_code=status.HTTP_200_OK, json={"job_id": job_id})
            else:
                return self._method_not_allowed_response
        # If a job should be canceled
        elif self._job_path_regex.match(request.url.path) is not None:
            # Route supports GET Method
            if request.method == "GET":
                # Parse job ID from url path
                job_id = int(request.url.path.split("/")[-1])
                if job_id >= len(self._request_bodies):
                    return Response(status_code=status.HTTP_404_NOT_FOUND, text=f"Job with ID {job_id} not found")
                # Get status of job for slurm job monitoring
                state = "FAILED" if self.send_error else "COMPLETED"
                return Response(
                    status_code=status.HTTP_200_OK,
                    json={"jobs": [{"job_state": state}]},
                )
            else:
                return self._method_not_allowed_response
        # Requested route is not mocked
        return Response(status_code=status.HTTP_404_NOT_FOUND, text="Unable find requested URL.")

    @staticmethod
    def authorize_request(headers: Headers) -> Response | None:
        """
        Check if the authorization headers are present in a request

        Parameters
        ----------
        headers : httpx.Headers
            Request headers

        Returns
        -------
        response : httpx.Response | None, default None
            Response with an error message if the request is not authorized, None otherwise.
        """
        if headers.get("X-SLURM-USER-TOKEN") is None or headers.get("X-SLURM-USER-NAME") is None:
            return Response(status_code=status.HTTP_401_UNAUTHORIZED, text="Authentication failure")
        return None

    def reset(self) -> None:
        """
        Resets the mock service to its initial state.
        """
        super().reset()
        self._request_bodies = []
        self._job_states = []

    def add_workflow_execution(self, job: SlurmRequestBody) -> int:
        """
        Add a workflow execution to the list.

        Parameters
        ----------
        job : dict[str, Any]
            The requests body for submitting a slurm job.

        Returns
        -------
        job_id : int
            The assigned job id.
        """
        self._request_bodies.append(job)
        self._job_states.append(True)
        return len(self._request_bodies) - 1

    def get_job(self, job_id: int) -> SlurmRequestBody:
        """
        Get a job by its ID.

        Parameters
        ----------
        job_id : int
            The ID of a job.

        Returns
        -------
        job : dict[str, Any]
            The request body of the job with the given ID.
        """
        if job_id < 0:
            raise IndexError("Index must be larger than 0")
        return self._request_bodies[job_id]

    def job_active(self, job_id: int) -> bool:
        """
        Check if a job is active or got canceled.

        Parameters
        ----------
        job_id : int
            The ID of a job.

        Returns
        -------
        job : bool
            Flag if the job is still active.
        """
        if job_id < 0:
            raise IndexError("Index must be larger than 0")
        return self._job_states[job_id]

    def get_job_by_name(self, job_name: UUID) -> SlurmRequestBody | None:
        """
        Get a job by its name.

        Parameters
        ----------
        job_name :  uuid.UUID
            Name of a job.

        Returns
        -------
        job : dict[str, Any] | None
            The request body of the job with the given name if it exists.
        """
        return next((job for job in self._request_bodies if job["job"]["name"] == job_name.hex), None)
