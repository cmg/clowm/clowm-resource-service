from datetime import datetime
from hashlib import md5
from io import BytesIO
from typing import TYPE_CHECKING, Any

from anyio import open_file
from botocore.exceptions import ClientError

if TYPE_CHECKING:
    from types_aiobotocore_s3.type_defs import CORSConfigurationTypeDef, FileobjTypeDef
else:
    CORSConfigurationTypeDef = object
    FileobjTypeDef = object


class MockS3Object:
    """
    Mock S3 object for the boto3 S3Object for testing purposes.

    Attributes
    ----------
    key : str
        Key of the S3 object.
    bucket_name : str
        Name of the corresponding bucket.

    """

    def __init__(self, bucket_name: str, key: str, content: bytes = b"") -> None:
        """
        Initialize a MockS3Object.

        Parameters
        ----------
        bucket_name : str
            Name of the corresponding bucket.
        key : str
            Key of the S3 object.
        """
        self.key = key
        self.bucket_name = bucket_name
        self.content_type = "text/plain"
        self.content = content
        self._etag = md5(content).hexdigest()

    @property
    async def content_length(self) -> int:
        return len(self.content)

    @property
    async def e_tag(self) -> str:
        return self._etag

    def __repr__(self) -> str:
        return f"MockS3Object(key={self.key}, bucket={self.bucket_name})"

    async def load(self) -> None:
        pass

    async def download_file(self, Filename: str) -> None:
        async with await open_file(Filename, "wb") as f:
            await f.write(self.content)

    async def download_fileobj(self, Fileobj: FileobjTypeDef) -> None:
        Fileobj.write(self.content)


class MockS3ObjectSummary:
    """
    Mock S3 object summary for the boto3 S3ObjectSummary for testing purposes.

    Functions
    ---------
    Object() -> MockS3Object
        Save a new bucket policy.

    Attributes
    ----------
    key : str
        Key of the S3 object.
    bucket_name : str
        Name of the corresponding bucket.
    size : int
        Size of object in bytes. Always 100.
    last_modified : datetime
        Time and date of last modification of this object.
    etag : str
        Hash of the object content
    """

    def __init__(self, bucket_name: str, key: str, content: bytes = b"") -> None:
        """
        Initialize a MockS3ObjectSummary.

        Parameters
        ----------
        bucket_name : str
            Name of the corresponding bucket.
        key : str
            Key of the S3 object.
        """
        self.key = key
        self.bucket_name = bucket_name
        self._size = len(content)
        self._last_modified = datetime.now()
        self.obj = MockS3Object(bucket_name, key, content)

    @property
    async def last_modified(self) -> datetime:
        return self._last_modified

    @property
    async def e_tag(self) -> str:
        return await self.obj.e_tag

    @property
    async def size(self) -> int:
        return await self.obj.content_length

    def __repr__(self) -> str:
        return f"MockS3ObjectSummary(key={self.key}, bucket={self.bucket_name})"

    async def Object(self) -> MockS3Object:
        """
        Get the S3 Object from the summary.

        Returns
        -------
        sObject : app.tests.mocks.mock_s3_resource.MockS3Object
            The corresponding S3Object.
        """
        return self.obj

    async def load(self) -> None:
        pass


class MockS3BucketPolicy:
    """
    Mock S3 bucket policy the boto3 BucketPolicy for testing purposes.

    Functions
    ---------
    put(Policy: str) -> None
        Save a new bucket policy.

    Attributes
    ----------
    bucket_name : str
        Name of the corresponding bucket.
    policy : str
        The policy in as a string.
    """

    def __init__(self, bucket_name: str):
        self.bucket_name = bucket_name
        self._policy: str = '{"Version": "2012-10-17", "Statement": []}'

    async def put(self, Policy: str) -> None:
        """
        Save a new bucket policy.

        Parameters
        ----------
        Policy : str
            The new policy as str.
        """
        self._policy = Policy

    async def load(self) -> None:
        pass

    async def delete(self) -> None:
        self._policy = '{"Version": "2012-10-17", "Statement": []}'

    @property
    async def policy(self) -> str:
        return self._policy


class MockS3CorsRule:
    """
    Mock S3 Cors Configuration for the boto3 BucketCors for testing purposes.

    Functions
    ---------
    put(CORSConfiguration: CORSConfig) -> None
        Save a new bucket CORS rule.

    Attributes
    ----------
    rules : str
        List of all CORS rules on the bucket.
    """

    def __init__(self) -> None:
        self.rules: CORSConfigurationTypeDef | None = None

    def put(self, CORSConfiguration: CORSConfigurationTypeDef) -> None:
        """
        Save a new bucket CORS rule.

        Parameters
        ----------
        CORSConfiguration : mypy_boto3_s3.type_defs.CORSConfigurationTypeDef
            The new policy as str.

        Notes
        -----
        A configuration has the following form
        {
            "CORSRules": [
                {
                    "ID": string,
                    "AllowedHeaders": list[string],
                    "AllowedMethods": list[string],
                    "AllowedOrigins": list[string],
                    "ExposeHeaders": list[string],
                    "MaxAgeSeconds": int,
                },
            ]
        }
        """
        self.rules = CORSConfiguration


class MockS3Bucket:
    """
    Mock S3 bucket for the boto3 Bucket for testing purposes.

    Functions
    ---------
    Policy() -> app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
        Get the bucket policy from the bucket.
    create() -> None
        Create the bucket in the mock service.
    delete() -> None
        Delete the bucket in the mock service
    delete_objects(Delete: dict[str, list[dict[str, str]]]) -> None
        Delete multiple objects in the bucket.
    get_objects() -> list[app.tests.mocks.mock_s3_resource.MockS3ObjectSummary]
        List of MockS3ObjectSummary in the bucket.
    add_object(obj: app.tests.mocks.mock_s3_resource.MockS3ObjectSummary) -> None
        Add a MockS3ObjectSummary to the bucket.

    Attributes
    ----------
    name: str
        name of the bucket.
    creation_date : datetime
        Creation date of the bucket.
    objects : app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
        Object in the bucket in a proxy class.
    """

    class MockS3ObjectList:
        """
        Proxy object to package a list in a class.

        Important because you have to access all the S3ObjectsSummary of a bucket with
        S3Bucket.objects.all()

        Functions
        ---------
        all() -> list[app.tests.mocks.mock_s3_resource.MockS3ObjectSummary]
            Get the saved list.
        filter(Prefix: str) -> app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
            Filter the object in the list by the prefix all their keys should have.
        add(obj: app.tests.mocks.mock_s3_resource.MockS3ObjectSummary) -> None
            Add a MockS3ObjectSummary to the list.
        delete(key: str) -> None
            Delete a MockS3ObjectSummary from the list
        """

        def __init__(self, parent_bucket: "MockS3Bucket", obj_list: list[MockS3ObjectSummary] | None = None) -> None:
            self._objs: list[MockS3ObjectSummary] = [] if obj_list is None else obj_list
            self._bucket = parent_bucket

        def all(self) -> list[MockS3ObjectSummary]:
            """
            Get the saved list.

            Returns
            -------
            objects : list[app.tests.mocks.mock_s3_resource.MockS3ObjectSummary]
                List of MockS3ObjectSummary
            """
            return self._objs

        def add(self, obj: MockS3ObjectSummary) -> None:
            """
            Add a MockS3ObjectSummary to the list.

            Parameters
            ----------
            obj : app.tests.mocks.mock_s3_resource.MockS3ObjectSummary
                MockS3ObjectSummary which should be added to the list
            """
            self._objs.append(obj)

        async def delete(self, key: str | None = None) -> list[dict[str, list[dict[str, str]]]]:
            """
            Delete a MockS3ObjectSummary from the list.

            Parameters
            ----------
            key : str
                Key of the object to delete
            """
            if key is not None:
                self._objs = [obj for obj in self._objs if obj.key != key]
                return [{"Deleted": [{"Key": key}]}]
            pre_deleted = self._objs.copy()
            await self._bucket.delete_objects({"Objects": [{"Key": obj.key} for obj in self._objs]})
            return [{"Deleted": [{"Key": obj.key} for obj in pre_deleted]}]

        def filter(self, Prefix: str) -> "MockS3Bucket.MockS3ObjectList":
            """
            Filter the object in the list by the prefix all their keys should have.

            Parameters
            ----------
            Prefix : str
                The prefix that all keys should have.

            Returns
            -------
            obj_list : app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
                The filtered list.
            """
            return MockS3Bucket.MockS3ObjectList(
                obj_list=[obj for obj in self._objs if obj.key.startswith(Prefix)], parent_bucket=self._bucket
            )

    def __init__(self, name: str, parent_service: "MockS3ServiceResource"):
        """
        Initialize a MockS3Bucket.

        Parameters
        ----------
        name : str
            Name of the bucket.
        parent_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock service object where the bucket will be saved.
        """
        self.name = name
        self.creation_date: datetime = datetime.now()
        self.objects = MockS3Bucket.MockS3ObjectList(parent_bucket=self)
        self._parent_service: MockS3ServiceResource = parent_service
        self.policy = MockS3BucketPolicy(name)
        self.cors = MockS3CorsRule()

    async def Policy(self) -> MockS3BucketPolicy:
        """
        Get the bucket policy from the bucket.

        Returns
        -------
        bucket_policy : app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
            The corresponding bucket policy.
        """
        return self.policy

    async def Cors(self) -> MockS3CorsRule:
        return self.cors

    async def create(self) -> None:
        """
        Create the bucket in the mock S3 service.
        """
        self._parent_service.create_bucket(self)

    async def delete(self) -> None:
        """
        Delete the bucket in the mock S3 service.
        """
        self._parent_service.delete_bucket(self.name)

    async def delete_objects(self, Delete: dict[str, list[dict[str, str]]]) -> None:
        """
        Delete multiple objects in the bucket.

        Parameters
        ----------
        Delete : dict[str, list[dict[str, str]]]
            The keys of the objects to delete.

        Notes
        -----
        The `Delete` parameter has the form
        {
            'Objects': [
                { 'Key' : 'test.txt' },...
            ]
        }
        """
        for key_object in Delete["Objects"]:
            await self.objects.delete(key=key_object["Key"])

    def get_objects(self) -> list[MockS3ObjectSummary]:
        """
        Get the MockS3ObjectSummary in the bucket.
        Convenience function for testing.

        Returns
        -------
        objects : list[app.tests.mocks.mock_s3_resource.MockS3ObjectSummary]
            List of MockS3ObjectSummary in the bucket.
        """
        return self.objects.all()

    def add_object(self, obj: MockS3ObjectSummary) -> None:
        """
        Add a MockS3ObjectSummary to the bucket.
        Convenience function for testing.

        Parameters
        ----------
        obj : app.tests.mocks.mock_s3_resource.MockS3ObjectSummary
            Object to add.
        """
        self.objects.add(obj)

    async def upload_fileobj(self, Fileobj: BytesIO, Key: str, **kwargs: Any) -> None:
        self.add_object(MockS3ObjectSummary(bucket_name=self.name, key=Key, content=Fileobj.read()))

    def __repr__(self) -> str:
        return f"MockS3Bucket(name={self.name}, objects={[obj.key for obj in self.get_objects()]})"


class MockS3ServiceResource:
    """
    A functional mock class of the boto3 S3ServiceResource for testing purposes.

    Functions
    ---------
    Bucket(name: str) -> app.tests.mocks.mock_s3_resource.MockS3bucket
        Get/Create a mock bucket.
    ObjectSummary(bucket_name: str, key: str) -> app.tests.mocks.mock_s3_resource.MockS3objectSummary
        Create a mock object summary.
    BucketPolicy(bucket_name: str) -> app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
        Get the bucket policy from the corresponding bucket.
    BucketAcl(bucket_name: str) -> app.tests.mocks.mock_s3_resource.MockS3BucketAcl
        Get the bucket acl from the corresponding bucket.
    create_bucket(bucket: app.tests.mocks.mock_s3_resource.MockS3bucket) -> None
        Create a bucket in the mock service.
    delete_bucket(name: str) -> None
        Delete a bucket in the mock service.
    create_object_in_bucket(bucket_name: str, key: str) -> app.tests.mocks.mock_s3_resource.MockS3objectSummary
        Create an MockS3ObjectSummary in a bucket.
    """

    def __init__(self) -> None:
        self._buckets: dict[str, MockS3Bucket] = {}

    async def Bucket(self, name: str) -> MockS3Bucket:
        """
        Get an existing bucket from the mock service or creat a new mock bucket but doesn't save it yet.
        Call `bucket.create()` on returned object for that.

        Parameters
        ----------
        name : str
            Name of the bucket.

        Returns
        -------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Existing/Created mock bucket.
        """
        return self._buckets[name] if name in self._buckets else MockS3Bucket(name=name, parent_service=self)

    async def ObjectSummary(self, bucket_name: str, key: str) -> MockS3ObjectSummary:
        """
        Create a mock object summary.

        Parameters
        ----------
        bucket_name : str
            Name of the corresponding bucket.
        key : str
            Key of the S3 object.

        Returns
        -------
        obj : app.tests.mocks.mock_s3_resource.MockS3objectSummary
              Existing MockS3ObjectSummary.

        Notes
        -----
        Raises an `botocore.exceptions.ClientError` if the object doesn't exist in the bucket.
        For creating a MockS3ObjectSummary see `create_object_in_bucket`.
        """
        if bucket_name not in self._buckets.keys():
            raise ClientError(operation_name="Mock", error_response={})
        for obj in self._buckets[bucket_name].objects.all():
            if obj.key == key:
                return obj
        raise ClientError(operation_name="Mock", error_response={})

    async def BucketPolicy(self, bucket_name: str) -> MockS3BucketPolicy:
        """
        Get the bucket policy from the corresponding bucket.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket.

        Returns
        -------
        bucket_policy : app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
            The corresponding bucket policy.
        """
        return self._buckets[bucket_name].policy

    def create_bucket(self, bucket: MockS3Bucket) -> None:
        """
        Create a bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Bucket which should be created.
        """
        self._buckets[bucket.name] = bucket

    def delete_bucket(self, name: str, force_delete: bool = False) -> None:
        """
        Delete am empty bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        name : str
            Name of the bucket.
        force_delete : bool, default False
            Force deletes even a non-empty bucket.
        """
        if name in self._buckets:
            if not force_delete and len(self._buckets[name].get_objects()) > 0:
                raise ClientError(operation_name="Mock", error_response={})
            del self._buckets[name]

    def create_object_in_bucket(self, bucket_name: str, key: str) -> MockS3ObjectSummary:
        """
        Create an MockS3ObjectSummary in a bucket.
        Convenience function for testing.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket.
        key : str
            key of the S3 object.

        Returns
        -------
        obj : app.tests.mocks.mock_s3_resource.MockS3ObjectSummary
            Newly created MockS3ObjectSummary.
        """
        obj = MockS3ObjectSummary(bucket_name=bucket_name, key=key)
        self._buckets[bucket_name].add_object(obj)
        return obj

    def __repr__(self) -> str:
        return f"MockS3ServiceResource(buckets={[bucket_name for bucket_name in self._buckets.keys()]})"
