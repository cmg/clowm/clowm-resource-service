import asyncio
import json
from contextlib import asynccontextmanager
from functools import partial
from io import BytesIO
from secrets import token_urlsafe
from typing import AsyncIterator, Iterator

import httpx
import pytest
import pytest_asyncio
from clowmdb.db.session import get_async_session
from clowmdb.models import Resource, ResourceVersion
from sqlalchemy import delete, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.api import dependencies
from app.api.dependencies import get_db, get_decode_jwt_function, get_httpx_client, get_s3_resource
from app.core.config import settings
from app.main import app
from app.schemas.resource_version import FileTree, resource_version_dir_name, resource_version_key
from app.tests.mocks import DefaultMockHTTPService, MockHTTPService
from app.tests.mocks.mock_opa_service import MockOpaService
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource
from app.tests.mocks.mock_slurm_cluster import MockSlurmCluster
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader, create_random_user, decode_mock_token, get_authorization_headers
from app.tests.utils.utils import random_lower_string

jwt_secret = token_urlsafe(32)


@pytest.fixture(scope="session")
def event_loop() -> Iterator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="session")
async def mock_s3_service() -> AsyncIterator[MockS3ServiceResource]:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    mock_s3 = MockS3ServiceResource()
    bucket = await mock_s3.Bucket(settings.s3.resource_bucket)
    await bucket.create()
    yield mock_s3
    mock_s3.delete_bucket(name=settings.s3.resource_bucket, force_delete=True)


@pytest.fixture(scope="session")
def mock_slurm_cluster() -> Iterator[MockSlurmCluster]:
    mock_slurm = MockSlurmCluster()
    yield mock_slurm
    mock_slurm.reset()


@pytest.fixture(scope="session")
def mock_opa_service() -> Iterator[MockOpaService]:
    mock_opa = MockOpaService()
    yield mock_opa
    mock_opa.reset()


@pytest_asyncio.fixture(scope="session")
async def mock_client(
    mock_opa_service: MockOpaService,
    mock_slurm_cluster: MockSlurmCluster,
) -> AsyncIterator[httpx.AsyncClient]:
    def mock_request_handler(request: httpx.Request) -> httpx.Response:
        url = str(request.url)
        handler: MockHTTPService
        if url.startswith(str(settings.cluster.slurm.uri)):
            handler = mock_slurm_cluster
        elif url.startswith(str(settings.opa.uri)):
            handler = mock_opa_service
        else:
            handler = DefaultMockHTTPService()
        return handler.handle_request(request)

    async with httpx.AsyncClient(transport=httpx.MockTransport(mock_request_handler)) as http_client:
        yield http_client


@pytest.fixture(autouse=True)
def monkeypatch_background_tasks(
    monkeypatch: pytest.MonkeyPatch,
    mock_client: httpx.AsyncClient,
    db: AsyncSession,
    mock_s3_service: MockS3ServiceResource,
) -> None:
    """
    Patch the functions to get resources in background tasks with mock resources.
    """

    @asynccontextmanager
    async def get_http_client() -> AsyncIterator[httpx.AsyncClient]:
        yield mock_client

    async def get_patch_db() -> AsyncIterator[AsyncSession]:
        yield db

    @asynccontextmanager
    async def get_s3() -> AsyncIterator[MockS3ServiceResource]:
        yield mock_s3_service

    monkeypatch.setattr(dependencies, "get_db", get_patch_db)
    monkeypatch.setattr(dependencies, "get_background_s3_resource", get_s3)
    monkeypatch.setattr(dependencies, "get_background_http_client", get_http_client)


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_s3_service: MockS3ServiceResource,
    db: AsyncSession,
    mock_opa_service: MockOpaService,
    mock_slurm_cluster: MockSlurmCluster,
    mock_client: httpx.AsyncClient,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides several dependencies.
    """

    app.dependency_overrides[get_httpx_client] = lambda: mock_client
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[get_decode_jwt_function] = lambda: partial(decode_mock_token, secret=jwt_secret)
    app.dependency_overrides[get_db] = lambda: db
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    async with get_async_session(url=str(settings.db.dsn_async), verbose=settings.db.verbose) as dbSession:
        yield dbSession


@pytest_asyncio.fixture(scope="function")
async def random_user(db: AsyncSession, mock_opa_service: MockOpaService) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_opa_service.add_user(user.lifescience_id, privileged=True)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_second_user(db: AsyncSession, mock_opa_service: MockOpaService) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random second user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_third_user(db: AsyncSession, mock_opa_service: MockOpaService) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random third user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource(
    db: AsyncSession,
    random_user: UserWithAuthHeader,
    mock_s3_service: MockS3ServiceResource,
    cleanup: CleanupList,
) -> AsyncIterator[Resource]:
    """
    Create a random resource and deletes it afterward.
    """
    resource_db = Resource(
        name=random_lower_string(8),
        short_description=random_lower_string(32),
        source=random_lower_string(32),
        maintainer_id_bytes=random_user.user.uid.bytes,
    )
    db.add(resource_db)
    await db.commit()
    resource_version_db = ResourceVersion(
        release=random_lower_string(8),
        resource_id_bytes=resource_db.resource_id.bytes,
        status=ResourceVersion.Status.LATEST,
    )
    db.add(resource_version_db)
    await db.commit()
    await db.refresh(resource_db, attribute_names=["versions"])

    s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
    policy = json.loads(await s3_policy.policy)
    policy["Statement"].append({"Sid": str(resource_db.resource_id)})
    await s3_policy.put(Policy=json.dumps(policy))
    cleanup.add_task(s3_policy.delete)

    yield resource_db
    await db.execute(delete(Resource).where(Resource.resource_id_bytes == resource_db.resource_id.bytes))
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource_version(db: AsyncSession, random_resource: Resource) -> ResourceVersion:
    """
    Create a random resource version and deletes it afterward.
    """
    resource_version: ResourceVersion = random_resource.versions[0]
    return resource_version


@pytest.fixture(scope="function", params=[status for status in ResourceVersion.Status])
def resource_state(request: pytest.FixtureRequest) -> ResourceVersion.Status:
    """
    Return all possible resource version sates as fixture
    """
    return request.param


@pytest_asyncio.fixture(scope="function")
async def random_resource_version_states(
    db: AsyncSession,
    random_resource: Resource,
    resource_state: ResourceVersion.Status,
    mock_s3_service: MockS3ServiceResource,
    cleanup: CleanupList,
) -> ResourceVersion:
    """
    Create a random resource version with all possible resource version status.
    """
    resource_version: ResourceVersion = random_resource.versions[0]
    stmt = (
        update(ResourceVersion)
        .where(ResourceVersion.resource_version_id_bytes == resource_version.resource_version_id.bytes)
        .values(status=resource_state)
    )
    if resource_state is ResourceVersion.Status.SYNC_REQUESTED:
        stmt = stmt.values(
            synchronization_request_description=random_lower_string(16),
            synchronization_request_uid_bytes=random_resource.maintainer_id_bytes,
        )
    await db.execute(stmt)
    await db.commit()
    await db.refresh(resource_version, attribute_names=["status"])

    if resource_state is ResourceVersion.Status.RESOURCE_REQUESTED:
        # create a permission for the maintainer to upload the resource to S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        policy = json.loads(await s3_policy.policy)
        policy["Statement"].append({"Sid": str(resource_version.resource_version_id)})
        await s3_policy.put(Policy=json.dumps(policy))

        # delete policy statement after the test
        async def delete_policy_stmt() -> None:
            policy["Statement"] = [
                s3_stmt
                for s3_stmt in policy["Statement"]
                if s3_stmt["Sid"] != str(resource_version.resource_version_id)
            ]
            await s3_policy.put(Policy=json.dumps(policy))

        cleanup.add_task(delete_policy_stmt)

    if resource_state in [
        ResourceVersion.Status.WAIT_FOR_REVIEW,
        ResourceVersion.Status.SYNC_REQUESTED,
        ResourceVersion.Status.SYNCHRONIZING,
        ResourceVersion.Status.SYNC_ERROR,
        ResourceVersion.Status.SYNCHRONIZED,
        ResourceVersion.Status.LATEST,
        ResourceVersion.Status.DENIED,
        ResourceVersion.Status.APPROVED,
    ]:
        # create the resource object in S3 for appropriate resource version states
        s3_bucket = await mock_s3_service.Bucket(settings.s3.resource_bucket)
        s3_key = resource_version_key(random_resource.resource_id, random_resource.versions[0].resource_version_id)
        await s3_bucket.upload_fileobj(
            BytesIO(b"content"),
            Key=s3_key,
        )
        cleanup.add_task(
            s3_bucket.delete_objects,
            Delete={"Objects": [{"Key": s3_key}]},
        )

    if resource_state in [
        ResourceVersion.Status.SYNCHRONIZED,
        ResourceVersion.Status.LATEST,
        ResourceVersion.Status.SETTING_LATEST,
    ]:
        # create the resource object in S3 for appropriate resource version states
        s3_bucket = await mock_s3_service.Bucket(settings.s3.resource_bucket)
        s3_key = (
            resource_version_dir_name(random_resource.resource_id, random_resource.versions[0].resource_version_id)
            + "/tree.json"
        )
        await s3_bucket.upload_fileobj(
            BytesIO(json.dumps([FileTree(type="file", name="some/path", size=10).model_dump()]).encode("utf-8")),
            Key=s3_key,
        )
        cleanup.add_task(
            s3_bucket.delete_objects,
            Delete={"Objects": [{"Key": s3_key}]},
        )

    return resource_version


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()
