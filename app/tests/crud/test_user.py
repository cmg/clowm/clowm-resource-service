from uuid import uuid4

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDUser
from app.tests.utils.user import UserWithAuthHeader


class TestUserCRUD:
    @pytest.mark.asyncio
    async def test_get_user(self, db: AsyncSession, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting a user from the database

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        user = await CRUDUser.get(db, random_user.user.uid)
        assert user is not None
        assert user == random_user.user

    @pytest.mark.asyncio
    async def test_get_non_existing_user(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing user from the database

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        user = await CRUDUser.get(db, uuid4())
        assert user is None

    @pytest.mark.asyncio
    async def test_get_user_by_lifescience_id(self, db: AsyncSession, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting a user from the database based on his lifescience id

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        users = await CRUDUser.get_by_lifescience_id(db, lifescience_ids=[random_user.user.lifescience_id])
        assert len(users) == 1
        assert users[0] == random_user.user
