from typing import Any, Callable, ParamSpec

from app.utils import Job

P = ParamSpec("P")


class CleanupList:
    """
    Helper object to hold a queue of functions that can be executed later
    """

    def __init__(self) -> None:
        self.queue: list[Job] = []

    def add_task(self, func: Callable[P, Any], *args: P.args, **kwargs: P.kwargs) -> None:
        """
        Add a (async) function to the queue.

        Parameters
        ----------
        func : Callable[P, Any]
            Function to register.
        args : P.args
            Arguments to the function.
        kwargs : P.kwargs
            Keyword arguments to the function.
        """
        self.queue.append(Job(func, *args, **kwargs))

    async def empty_queue(self) -> None:
        """
        Empty the queue by executing the registered functions.
        """
        while len(self.queue) > 0:
            func = self.queue.pop()
            if func.is_async:
                await func()
            else:
                func()
