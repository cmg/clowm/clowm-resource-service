from dataclasses import dataclass
from datetime import UTC, datetime, timedelta
from uuid import UUID

from authlib.jose import JsonWebToken
from clowmdb.models import User
from sqlalchemy.ext.asyncio import AsyncSession

from .utils import random_hex_string, random_lower_string

_jwt = JsonWebToken(["HS256"])


@dataclass
class UserWithAuthHeader:
    auth_headers: dict[str, str]
    user: User


def get_authorization_headers(uid: UUID, secret: str = "SuperSecret") -> dict[str, str]:
    """
    Create a valid JWT and return the correct headers for subsequent requests.

    Parameters
    ----------
    uid : str
        UID of the user who should be logged in.
    secret : str
        Secret to sign the JWT with
    Returns
    -------
    headers : dict[str,str]
        HTTP Headers to authorize each request.
    """
    to_encode = {"sub": str(uid), "exp": datetime.now(UTC) + timedelta(hours=1)}
    encoded_jwt = _jwt.encode(header={"alg": "HS256"}, payload=to_encode, key=secret)

    headers = {"Authorization": f"Bearer {encoded_jwt.decode('utf-8')}"}
    return headers


def decode_mock_token(token: str, secret: str = "SuperSecret") -> dict[str, str]:
    """
    Decode and verify a test JWT token.

    Parameters
    ----------
    token : str
        The JWT to decode.
    secret : str
        Secret to use for signature verification

    Returns
    -------
    decoded_token : dict[str, str]
        Payload of the decoded token.
    """
    claims = _jwt.decode(
        s=token,
        key=secret,
        claims_options={
            "sub": {"essential": True},
            "exp": {"essential": True},
        },
    )
    claims.validate()
    return claims


async def create_random_user(db: AsyncSession) -> User:
    """
    Creates a random user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.

    Returns
    -------
    user : clowmdb.models.User
        Newly created user.
    """
    user = User(
        lifescience_id=random_hex_string(),
        display_name=random_lower_string(),
    )
    db.add(user)
    await db.commit()
    return user
