from datetime import datetime
from uuid import UUID

from clowmdb.models import Bucket, BucketPermission, User
from sqlalchemy.ext.asyncio import AsyncSession

from .utils import random_lower_string


async def create_random_bucket(db: AsyncSession, user: User) -> Bucket:
    """
    Creates a random bucket in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    user : clowmdb.models.User
        Owner of the bucket.

    Returns
    -------
    bucket : clowmdb.models.Bucket
        Newly created bucket.
    """
    bucket = Bucket(
        name=random_lower_string(),
        description=random_lower_string(length=127),
        _owner_id=user.uid.bytes,
    )
    db.add(bucket)
    await db.commit()
    return bucket


async def add_permission_for_bucket(
    db: AsyncSession,
    bucket_name: str,
    uid: UUID,
    from_: datetime | None = None,
    to: datetime | None = None,
    permission: BucketPermission.Permission = BucketPermission.Permission.READWRITE,
    file_prefix: str | None = None,
) -> None:
    """
    Creates Permission to a bucket for a user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    bucket_name : str
        name of the bucket.
    uid : str
        UID of the user who we grant the permission.
    from_ : datetime.datetime | None, default None
        Time when from when the permission should be active.
    to : datetime.datetime | None, default None
        Time till when the permissions should be active.
    permission : clowmdb.models.BucketPermission.Permission, default BucketPermission.Permission.READWRITE  # noqa:E501
        The permission the user is granted.
    file_prefix : str | None, default None
        The file prefix for the permission.
    """
    perm = BucketPermission(
        uid_bytes=uid.bytes,
        bucket_name=bucket_name,
        from_=round(from_.timestamp()) if from_ is not None else None,
        to=round(to.timestamp()) if to is not None else None,
        permissions=permission.name,
        file_prefix=file_prefix,
    )
    db.add(perm)
    await db.commit()
