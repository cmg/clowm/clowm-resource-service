import json
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from clowmdb.models import Resource, ResourceVersion
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.schemas.resource import ResourceIn, ResourceOut
from app.schemas.resource_version import UserSynchronizationRequestOut, resource_version_key
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_lower_string


class _TestResourceRoutes:
    base_path: str = "/resources"


class TestResourceRouteCreate(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_create_resource_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        db: AsyncSession,
        cleanup: CleanupList,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for creating a new resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_lower_string(8),
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(self.base_path, json=resource.model_dump(), headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        resource_out = ResourceOut.model_validate_json(response.content)

        async def delete_resource() -> None:
            await db.execute(delete(Resource).where(Resource.resource_id_bytes == resource_out.resource_id.bytes))
            await db.commit()

        cleanup.add_task(delete_resource)
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        cleanup.add_task(s3_policy.delete)

        assert resource_out.name == resource.name
        assert len(resource_out.versions) == 1
        resource_version = resource_out.versions[0]
        assert resource_version.resource_id == resource_out.resource_id
        assert resource_version.status == ResourceVersion.Status.RESOURCE_REQUESTED
        assert resource_version.release == resource.release

        # test if the maintainer get permission to upload to the resource bucket
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        policy = json.loads(await s3_policy.policy)
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_out.resource_id)) == 1
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_version.resource_version_id)) == 1

        # test that the resource got actually created
        resource_db = await db.scalar(
            select(Resource).where(Resource.resource_id_bytes == resource_out.resource_id.bytes)
        )
        assert resource_db is not None

    @pytest.mark.asyncio
    async def test_create_duplicated_resource_route(
        self, client: AsyncClient, random_user: UserWithAuthHeader, db: AsyncSession, random_resource: Resource
    ) -> None:
        """
        Test for creating a duplicated resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_resource.name,
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(self.base_path, json=resource.model_dump(), headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestResourceRouteDelete(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_delete_resource_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for deleting a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_resource.resource_id)}", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # test if all permission for the maintainer got deleted in S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        policy = json.loads(await s3_policy.policy)
        assert (
            sum(
                1
                for stmt in policy["Statement"]
                if stmt["Sid"]
                in [str(random_resource.resource_id)] + [str(rv.resource_version_id) for rv in random_resource.versions]
            )
            == 0
        )

        # test if the resource got deleted in S3
        s3_key = resource_version_key(random_resource.resource_id, random_resource.versions[0].resource_version_id)
        with pytest.raises(ClientError):
            obj = await mock_s3_service.ObjectSummary(settings.s3.resource_bucket, s3_key)
            await obj.load()


class TestResourceRouteGet(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_list_resources_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for listing all resources.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(self.base_path, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_list_resource_sync_requests_route(
        self, client: AsyncClient, random_resource_version_states: ResourceVersion, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for listing all resource sync requests.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource_version_states : clowmdb.models.ResourceVersion
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/sync_requests", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[UserSynchronizationRequestOut])
        requests = ta.validate_json(response.content)
        if random_resource_version_states.status is not ResourceVersion.Status.SYNC_REQUESTED:
            assert len(requests) == 0
            return
        assert len(requests) == 1
        request = requests[0]
        assert request.resource_id == random_resource_version_states.resource_id
        assert request.resource_version_id == random_resource_version_states.resource_version_id
        assert request.reason == random_resource_version_states.synchronization_request_description
        assert random_resource_version_states.synchronization_request_uid is not None

    @pytest.mark.asyncio
    async def test_get_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a specific resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource.resource_id)}", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a non-existing resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{str(uuid4())}", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND
