import pytest
from fastapi import status
from httpx import AsyncClient


class TestHealthRoute:
    @pytest.mark.asyncio
    async def test_health_route(self, client: AsyncClient) -> None:
        """
        Test service health route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get("/health")
        assert response.status_code == status.HTTP_200_OK
        body = response.json()
        assert body["status"] == "OK"


class TestOpenAPIRoute:
    @pytest.mark.asyncio
    async def test_openapi_route(self, client: AsyncClient) -> None:
        """
        Test for getting the OpenAPI specification and the caching mechanism.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response1 = await client.get("/openapi.json")
        assert response1.status_code == status.HTTP_200_OK
        assert response1.headers.get("ETag") is not None

        response2 = await client.get("/openapi.json", headers={"If-None-Match": response1.headers.get("ETag")})
        assert response2.status_code == status.HTTP_304_NOT_MODIFIED

    @pytest.mark.asyncio
    async def test_swagger_ui_route(self, client: AsyncClient) -> None:
        """
        Test for getting the Swagger UI.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response1 = await client.get("/docs")
        assert response1.status_code == status.HTTP_200_OK
