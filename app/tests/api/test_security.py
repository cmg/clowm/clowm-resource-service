import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.tests.mocks.mock_opa_service import MockOpaService
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader


class TestJWTProtectedRoutes:
    protected_route: str = "/resources"

    @pytest.mark.asyncio
    async def test_missing_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with missing authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route)
        error = response.json()
        assert error
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert error["detail"] == "Not authenticated"

    @pytest.mark.asyncio
    async def test_malformed_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with malformed authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route, headers={"Authorization": "Bearer not-a-jwt-token"})
        error = response.json()
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert error
        assert error["detail"] == "Malformed JWT"

    @pytest.mark.asyncio
    async def test_correct_authorization_header(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test with correct authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            self.protected_route, params={"maintainer_id": str(random_user.user.uid)}, headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_protected_route_with_deleted_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test with correct authorization header from a deleted user on a protected route.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        await db.delete(random_user.user)
        await db.commit()

        response = await client.get(
            self.protected_route, params={"user": str(random_user.user.uid)}, headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_routed_with_insufficient_permissions(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_opa_service: MockOpaService,
        cleanup: CleanupList,
    ) -> None:
        """
        Test with correct authorization header but with insufficient permissions.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """

        mock_opa_service.send_error = True

        def reset_opa_service() -> None:
            mock_opa_service.send_error = False

        cleanup.add_task(reset_opa_service)

        response = await client.get(
            self.protected_route,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
