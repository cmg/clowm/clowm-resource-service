import json
from copy import copy
from io import BytesIO
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from clowmdb.models import Resource, ResourceVersion
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.schemas.resource_version import (
    ResourceVersionIn,
    ResourceVersionOut,
    UserRequestAnswer,
    UserSynchronizationRequestIn,
    resource_version_dir_name,
    resource_version_key,
)
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource
from app.tests.mocks.mock_slurm_cluster import MockSlurmCluster
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_lower_string


class _TestResourceVersionRoutes:
    base_path: str = "/resources"


class TestResourceVersionRouteCreate(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_create_resource_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_resource: Resource,
        cleanup: CleanupList,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for creating a new resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        resource_version = ResourceVersionIn(release=random_lower_string(6))
        response = await client.post(
            f"{self.base_path}/{str(random_resource.resource_id)}/versions",
            content=resource_version.model_dump_json(),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_201_CREATED
        resource_version_out = ResourceVersionOut.model_validate_json(response.content)

        # Load bucket policy
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        policy = json.loads(await s3_policy.policy)

        # delete create bucket policy statement after the test
        async def delete_policy_stmt() -> None:
            policy["Statement"] = [
                stmt for stmt in policy["Statement"] if stmt["Sid"] != str(resource_version_out.resource_version_id)
            ]
            await s3_policy.put(json.dumps(policy))

        cleanup.add_task(delete_policy_stmt)

        # check if a bucket policy statement was created
        assert (
            sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_version_out.resource_version_id)) == 1
        )


class TestResourceVersionRouteGet(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_list_resource_versions_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for listing all resource versions of a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version : clowmdb.models.Resource
            Random resource version for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_resource.resource_id), "versions"]), headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting a specif resource version of a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version : clowmdb.models.Resource
            Random resource version for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version.resource_version_id),
                ]
            ),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a non-existing resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource.resource_id)}/versions/{str(uuid4())}",
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_resource_version_folder_tree(
        self, client: AsyncClient, random_resource_version_states: ResourceVersion, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting the folder structure of a resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_resource_version_states : clowmdb.models.ResourceVersion
            Random resource version with all possible states for testing .
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource_version_states.resource_id)}/versions/{str(random_resource_version_states.resource_version_id)}/tree",
            headers=random_user.auth_headers,
        )
        if random_resource_version_states.status.public:
            if random_resource_version_states.status in [
                ResourceVersion.Status.SYNCHRONIZED,
                ResourceVersion.Status.LATEST,
                ResourceVersion.Status.SETTING_LATEST,
            ]:
                assert response.status_code == status.HTTP_200_OK
                response2 = await client.get(
                    f"{self.base_path}/{str(random_resource_version_states.resource_id)}/versions/{str(random_resource_version_states.resource_version_id)}/tree",
                    headers={**random_user.auth_headers, "if-none-match": response.headers.get("etag", "")},
                )
                assert response2.status_code == status.HTTP_304_NOT_MODIFIED
            else:
                assert response.status_code == status.HTTP_404_NOT_FOUND

        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_get_resource_version_folder_tree_large_file(
        self,
        client: AsyncClient,
        random_resource_version: ResourceVersion,
        random_user: UserWithAuthHeader,
        cleanup: CleanupList,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for getting the folder structure of a resource version when the file is larger than 10MiB.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_resource_version : clowmdb.models.ResourceVersion
            Random resource version for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        # put large file containing the directory structure in S3
        s3_bucket = await mock_s3_service.Bucket(settings.s3.resource_bucket)
        s3_key = (
            resource_version_dir_name(random_resource_version.resource_id, random_resource_version.resource_version_id)
            + "/tree.json"
        )
        await s3_bucket.upload_fileobj(
            BytesIO(b"0" * 10500000),
            Key=s3_key,
        )

        cleanup.add_task(
            s3_bucket.delete_objects,
            Delete={"Objects": [{"Key": s3_key}]},
        )
        response = await client.get(
            f"{self.base_path}/{str(random_resource_version.resource_id)}/versions/{str(random_resource_version.resource_version_id)}/tree",
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK


class TestResourceVersionRouteDelete(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_delete_resource_version_cluster_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for deleting a resource version on the cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        previous_state = copy(random_resource_version_states.status)
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "cluster",
                ]
            ),
            headers=random_user.auth_headers,
        )
        if previous_state in [
            ResourceVersion.Status.SYNCHRONIZED,
            ResourceVersion.Status.LATEST,
            ResourceVersion.Status.CLUSTER_DELETE_ERROR,
        ]:
            assert response.status_code == status.HTTP_200_OK
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_delete_resource_version_s3_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for deleting a resource version in the S3 Bucket

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        previous_state = copy(random_resource_version_states.status)
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "s3",
                ]
            ),
            headers=random_user.auth_headers,
        )
        if previous_state in [
            ResourceVersion.Status.APPROVED,
            ResourceVersion.Status.DENIED,
            ResourceVersion.Status.S3_DELETE_ERROR,
        ]:
            assert response.status_code == status.HTTP_200_OK
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST
            return

        # test that the resource is deleted in S3
        with pytest.raises(ClientError):
            obj = await mock_s3_service.ObjectSummary(
                bucket_name=settings.s3.resource_bucket,
                key=resource_version_key(
                    random_resource.resource_id, random_resource_version_states.resource_version_id
                ),
            )
            await obj.load()

        # test that the maintainer has no permission to upload a new resource to S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
        policy = json.loads(await s3_policy.policy)
        assert (
            sum(
                1
                for stmt in policy["Statement"]
                if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
            )
            == 0
        )


class TestResourceVersionRoutePut(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_request_sync_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for requesting a synchronization of the resource version to the cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        previous_state = copy(random_resource_version_states.status)

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "request_sync",
                ]
            ),
            content=UserSynchronizationRequestIn(reason=random_lower_string(32)).model_dump_json(),
            headers=random_user.auth_headers,
        )

        if previous_state is ResourceVersion.Status.APPROVED:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.SYNC_REQUESTED
            # test if the maintainer gets a permission to upload to the bucket
            s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
            policy = json.loads(await s3_policy.policy)
            assert (
                sum(
                    1
                    for stmt in policy["Statement"]
                    if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
                )
                == 0
            )
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_sync_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for synchronizing a resource version to the cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        previous_status = copy(random_resource_version_states.status)
        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "sync",
                ]
            ),
            content=UserRequestAnswer().model_dump_json(),
            headers=random_user.auth_headers,
        )
        if previous_status in [
            ResourceVersion.Status.SYNC_REQUESTED,
            ResourceVersion.Status.APPROVED,
            ResourceVersion.Status.SYNC_ERROR,
        ]:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.SYNCHRONIZING
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_deny_sync_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for denying a synchronization request of the resource version to the cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        previous_state = copy(random_resource_version_states.status)
        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "sync",
                ]
            ),
            content=UserRequestAnswer(deny=True, reason=random_lower_string(32)).model_dump_json(),
            headers=random_user.auth_headers,
        )

        if previous_state == ResourceVersion.Status.SYNC_REQUESTED:
            assert response.status_code == status.HTTP_200_OK
            updated_version = ResourceVersionOut.model_validate_json(response.content)
            assert updated_version.status == ResourceVersion.Status.APPROVED
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_fail_sync_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
    ) -> None:
        """
        Test for fail synchronizing a resource version to the cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        previous_status = copy(random_resource_version_states.status)
        mock_slurm_cluster.send_error = True

        def reset_slurm_config() -> None:
            mock_slurm_cluster.send_error = False

        cleanup.add_task(reset_slurm_config)
        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "sync",
                ]
            ),
            content=UserRequestAnswer().model_dump_json(),
            headers=random_user.auth_headers,
        )
        if previous_status in [
            ResourceVersion.Status.SYNC_REQUESTED,
            ResourceVersion.Status.APPROVED,
            ResourceVersion.Status.SYNC_ERROR,
        ]:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.SYNCHRONIZING
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST
            return

        # reset db connection to clear cache
        await db.reset()
        db_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id_bytes == random_resource_version_states.resource_version_id_bytes
            )
        )
        assert db_version is not None
        assert db_version.status is ResourceVersion.Status.SYNC_ERROR

    @pytest.mark.asyncio
    async def test_request_review_resource_version_route_without_uploaded_resource(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for requesting a review of a resource version without uploaded resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        s3_bucket = await mock_s3_service.Bucket(settings.s3.resource_bucket)
        s3_key = resource_version_key(random_resource.resource_id, random_resource_version_states.resource_version_id)
        await s3_bucket.delete_objects({"Objects": [{"Key": s3_key}]})

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "request_review",
                ]
            ),
            content=UserSynchronizationRequestIn(reason=random_lower_string(32)).model_dump_json(),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_request_review_resource_version_route_with_uploaded_resource(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for requesting a review of a resource version with uploaded resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        previous_state = random_resource_version_states.status
        if previous_state == ResourceVersion.Status.RESOURCE_REQUESTED:
            s3_bucket = await mock_s3_service.Bucket(settings.s3.resource_bucket)
            s3_key = resource_version_key(
                random_resource.resource_id, random_resource_version_states.resource_version_id
            )
            await s3_bucket.upload_fileobj(
                BytesIO(b"content"),
                Key=s3_key,
            )
            cleanup.add_task(
                s3_bucket.delete_objects,
                Delete={"Objects": [{"Key": s3_key}]},
            )

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "request_review",
                ]
            ),
            content=UserSynchronizationRequestIn(reason=random_lower_string(32)).model_dump_json(),
            headers=random_user.auth_headers,
        )

        if previous_state is ResourceVersion.Status.RESOURCE_REQUESTED:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.WAIT_FOR_REVIEW
            # test if the maintainer gets a permission to upload to the bucket
            s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
            policy = json.loads(await s3_policy.policy)
            assert (
                sum(
                    1
                    for stmt in policy["Statement"]
                    if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
                )
                == 0
            )
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_review_resource_version(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for accepting a review of the resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        previous_state = random_resource_version_states.status

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "review",
                ]
            ),
            content=UserRequestAnswer(deny=False).model_dump_json(),
            headers=random_user.auth_headers,
        )

        if previous_state is ResourceVersion.Status.WAIT_FOR_REVIEW:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.APPROVED
            # test if the maintainer gets a permission to upload to the bucket
            s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
            policy = json.loads(await s3_policy.policy)
            assert (
                sum(
                    1
                    for stmt in policy["Statement"]
                    if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
                )
                == 0
            )
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_deny_review_resource_version(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for rejecting a review of the resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        previous_state = random_resource_version_states.status

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "review",
                ]
            ),
            content=UserRequestAnswer(deny=True, reason=random_lower_string(32)).model_dump_json(),
            headers=random_user.auth_headers,
        )

        if previous_state is ResourceVersion.Status.WAIT_FOR_REVIEW:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.DENIED
            # test if the maintainer gets a permission to upload to the bucket
            s3_policy = await mock_s3_service.BucketPolicy(settings.s3.resource_bucket)
            policy = json.loads(await s3_policy.policy)
            assert (
                sum(
                    1
                    for stmt in policy["Statement"]
                    if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
                )
                == 0
            )
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_set_latest_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for setting a resource version as the latest version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        previous_state = random_resource_version_states.status
        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "latest",
                ]
            ),
            headers=random_user.auth_headers,
        )
        if previous_state == ResourceVersion.Status.SYNCHRONIZED:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.SETTING_LATEST
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_fail_set_latest_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        random_user: UserWithAuthHeader,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
    ) -> None:
        """
        Test for setting a resource version as the latest version fail

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowmdb.models.Resource
            Random resource for testing.
        random_resource_version_states : clowmdb.models.Resource
            Random resource version with all possible states for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        previous_state = copy(random_resource_version_states.status)
        mock_slurm_cluster.send_error = True

        def reset_slurm_config() -> None:
            mock_slurm_cluster.send_error = False

        cleanup.add_task(reset_slurm_config)

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "latest",
                ]
            ),
            headers=random_user.auth_headers,
        )
        if previous_state == ResourceVersion.Status.SYNCHRONIZED:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.Status.SETTING_LATEST
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST
            return

        # reset db connection to clear cache
        await db.reset()
        db_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id_bytes == random_resource_version_states.resource_version_id_bytes
            )
        )
        assert db_version is not None
        assert db_version.status is ResourceVersion.Status.SYNCHRONIZED
