from uuid import UUID

from clowmdb.models import Resource, ResourceVersion
from opentelemetry import trace
from sqlalchemy import delete, or_, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from app.crud.crud_resource_version import CRUDResourceVersion
from app.schemas.resource import ResourceIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDResource:
    @staticmethod
    async def get(db: AsyncSession, resource_id: UUID) -> Resource | None:
        """
        Get a resource by its ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_id : uuid.UUID
            ID of a resource.

        Returns
        -------
        resource : clowmdb.models.Resource | None
            The resource with the given ID if it exists, None otherwise
        """

        stmt = (
            select(Resource)
            .where(Resource.resource_id_bytes == resource_id.bytes)
            .options(joinedload(Resource.versions))
        )
        with tracer.start_as_current_span(
            "db_get_resource", attributes={"resource_id": str(resource_id), "sql_query": str(stmt)}
        ):
            return await db.scalar(stmt)

    @staticmethod
    async def get_by_name(db: AsyncSession, name: str) -> Resource | None:
        """
        Get a resource by its name.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        name: str
            Name of a resource.

        Returns
        -------
        resource : clowmdb.models.Resource | None
            The resource with the given name if it exists, None otherwise
        """

        stmt = select(Resource).where(Resource.name == name)
        with tracer.start_as_current_span("db_get_resource_by_name", attributes={"name": name, "sql_query": str(stmt)}):
            return await db.scalar(stmt)

    @staticmethod
    async def list_resources(
        db: AsyncSession,
        name_substring: str | None = None,
        maintainer_id: UUID | None = None,
        version_status: list[ResourceVersion.Status] | None = None,
        public: bool | None = None,
    ) -> list[Resource]:
        """
        List all resources. Populates the version attribute of the resources.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        name_substring : str | None, default None
            Substring to filter for in the name of a resource.
        maintainer_id : uuid.UUID | None, default None
            Filter resources by maintainer.
        version_status : list[clowmdb.models.ResourceVersion.Status] | None, default None
            Filter versions of a resource based on the status. Removes resources that have no version after this filter.
        public : bool | None, default None
            Filter resources to include only public resources.

        Returns
        -------
        workflows : list[clowmdb.models.Resource]
            List of resources.
        """
        with tracer.start_as_current_span("db_list_resources") as span:
            stmt = select(Resource).options(joinedload(Resource.versions))
            if name_substring is not None:
                span.set_attribute("name_substring", name_substring)
                stmt = stmt.where(Resource.name.contains(name_substring))
            if maintainer_id is not None:
                span.set_attribute("maintainer_id", str(maintainer_id))
                stmt = stmt.where(Resource.maintainer_id_bytes == maintainer_id.bytes)
            if public is not None:
                span.set_attribute("public", public)
                stmt = stmt.where(Resource.private != public)
            if version_status is not None and len(version_status) > 0:
                span.set_attribute("status", [status.name for status in version_status])
                stmt = stmt.options(
                    joinedload(
                        Resource.versions.and_(or_(*[ResourceVersion.status == status for status in version_status]))
                    )
                ).execution_options(populate_existing=True)
            span.set_attribute("sql_query", str(stmt))
            return [w for w in (await db.scalars(stmt)).unique().all() if len(w.versions) > 0]

    @staticmethod
    async def delete(db: AsyncSession, resource_id: UUID) -> None:
        """
        Delete a resource.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_id : uuid.UUID
            ID of a resource
        """
        stmt = delete(Resource).where(Resource.resource_id_bytes == resource_id.bytes)
        with tracer.start_as_current_span(
            "db_delete_resource", attributes={"resource_id": str(resource_id), "sql_query": str(stmt)}
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def create(db: AsyncSession, resource_in: ResourceIn, maintainer_id: UUID) -> Resource:
        """
        Create a new resource.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        resource_in : app.schemas.resource.ResourceIn
            Data about the new resource.
        maintainer_id : str
            UID of the maintainer.

        Returns
        -------
        resource : clowmdb.models.Resource
            The newly created resource
        """
        with tracer.start_as_current_span(
            "db_create_resource",
            attributes={"maintainer_id": str(maintainer_id), "resource_in": resource_in.model_dump_json(indent=2)},
        ) as span:
            resource_db = Resource(
                name=resource_in.name,
                short_description=resource_in.description,
                source=resource_in.source,
                maintainer_id_bytes=maintainer_id.bytes,
                private=resource_in.private,
            )
            db.add(resource_db)
            await db.commit()
            span.set_attribute("resource_id", str(resource_db.resource_id))
            await CRUDResourceVersion.create(db, resource_id=resource_db.resource_id, release=resource_in.release)
            return await CRUDResource.get(db, resource_db.resource_id)
