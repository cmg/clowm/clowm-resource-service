from typing import Sequence
from uuid import UUID

from clowmdb.models import User
from opentelemetry import trace
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDUser:
    @staticmethod
    async def get(db: AsyncSession, uid: UUID) -> User | None:
        """
        Get a user by its UID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        uid : uuid.UUID
            UID of a user.

        Returns
        -------
        user : clowmdb.models.User | None
            The user for the given UID if he exists, None otherwise
        """
        with tracer.start_as_current_span("db_get_user", attributes={"uid": str(uid)}) as span:
            stmt = select(User).where(User.uid_bytes == uid.bytes)
            span.set_attribute("sql_query", str(stmt))
            return await db.scalar(stmt)

    @staticmethod
    async def get_by_lifescience_id(db: AsyncSession, lifescience_ids: list[str]) -> Sequence[User]:
        """
        Get users by their lifescience id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        lifescience_ids : list[str]
            List of lifescience ids

        Returns
        -------
        user : list[clowmdb.models.User]
            The users for the given lifescience ids
        """
        stmt = select(User).where(User.lifescience_id.in_(lifescience_ids))
        with tracer.start_as_current_span("db_get_user_lifescience", attributes={"sql_query": str(stmt)}):
            return (await db.scalars(stmt)).unique().all()
