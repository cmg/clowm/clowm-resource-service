from typing import Any
from uuid import UUID

from clowmdb.models import ResourceVersion
from opentelemetry import trace
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import text

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDResourceVersion:
    @staticmethod
    async def create(db: AsyncSession, resource_id: UUID, release: str) -> ResourceVersion:
        """
        Create a new resource version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
        resource_id : uuid.UUID
            ID of the resource.
        release : str
            Versions of the resource.

        Returns
        -------
        resource_version : clowmdb.models.ResourceVersion
            The newly created resource version
        """
        with tracer.start_as_current_span(
            "db_create_resource_version", attributes={"resource_id": str(resource_id), "release": release}
        ) as span:
            resource_version_db = ResourceVersion(release=release, resource_id_bytes=resource_id.bytes)
            db.add(resource_version_db)
            await db.commit()
            span.set_attribute("resource_version_id", str(resource_version_db.resource_version_id))
            return resource_version_db

    @staticmethod
    async def update_status(
        db: AsyncSession,
        status: ResourceVersion.Status,
        resource_version_id: UUID,
        resource_id: UUID | None = None,
        slurm_job_id: int | None = None,
        values: dict[str, Any] | None = None,
    ) -> None:
        """
        Update the status of a resource version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_version_id : uuid.UUID
            ID of a resource version.
        resource_id : uuid.UUID | None, default None
            ID of a resource. Must be set if `status` is LATEST.
        status : clowmdb.models.ResourceVersion.Status
            New status of the resource version
        slurm_job_id : int | None, default None
            Slurm job id if the update comes from a executed slurm job
        values : dict[str, Any] | None, defualt None
            Allow to set arbitrary values for the update statement
        """
        if status == ResourceVersion.Status.LATEST:
            if resource_id is None:
                raise ValueError("Parameter 'resource_version_id' or 'resource_id' must not be None")
            stmt1 = (
                update(ResourceVersion)
                .values(
                    status=ResourceVersion.Status.SYNCHRONIZED.name, remove_latest_timestamp=text("UNIX_TIMESTAMP()")
                )
                .where(ResourceVersion.resource_id_bytes == resource_id.bytes)
            )
            with tracer.start_as_current_span(
                "db_remove_latest_tag",
                attributes={"status": status.name, "resource_id": str(resource_id), "sql_query": str(stmt1)},
            ):
                await db.execute(stmt1)

        stmt2 = (
            update(ResourceVersion)
            .values(status=status.name)
            .where(ResourceVersion.resource_version_id_bytes == resource_version_id.bytes)
        )
        if values:
            stmt2 = stmt2.values(**values)
        if status == ResourceVersion.Status.SYNCHRONIZED:
            stmt2 = stmt2.values(sync_finished_timestamp=text("UNIX_TIMESTAMP()"))
        elif status == ResourceVersion.Status.LATEST:
            stmt2 = stmt2.values(set_latest_timestamp=text("UNIX_TIMESTAMP()"))
        elif status == ResourceVersion.Status.S3_DELETED:
            stmt2 = stmt2.values(s3_deleted_timestamp=text("UNIX_TIMESTAMP()"))

        if slurm_job_id is not None:
            if status == ResourceVersion.Status.SYNCHRONIZING:
                stmt2 = stmt2.values(sync_slurm_job_id=slurm_job_id)
            elif status == ResourceVersion.Status.CLUSTER_DELETING:
                stmt2 = stmt2.values(cluster_delete_slurm_job_id=slurm_job_id)
            elif status == ResourceVersion.Status.SETTING_LATEST:
                stmt2 = stmt2.values(set_latest_slurm_job_id=slurm_job_id)
        with tracer.start_as_current_span(
            "db_update_resource_version_status",
            attributes={
                "status": status.name,
                "resource_version_id": str(resource_version_id),
                "sql_query": str(stmt2),
            },
        ):
            await db.execute(stmt2)
            await db.commit()

    @staticmethod
    async def get(
        db: AsyncSession, resource_version_id: UUID, resource_id: UUID | None = None
    ) -> ResourceVersion | None:
        """
        Get a resource version by its ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_version_id : uuid.UUID
            ID of a resource version.
        resource_id : uuid.UUID | None
            ID of a resource

        Returns
        -------
        resource_version : clowmdb.models.ResourceVersion | None
            The resource version with the given ID if it exists, None otherwise
        """
        stmt = select(ResourceVersion).where(ResourceVersion.resource_version_id_bytes == resource_version_id.bytes)
        with tracer.start_as_current_span(
            "db_get_resource_version",
            attributes={
                "resource_version_id": str(resource_version_id),
            },
        ) as span:
            if resource_id:
                span.set_attribute("resource_id", str(resource_id))
                stmt = stmt.where(ResourceVersion.resource_id_bytes == resource_id.bytes)
            span.set_attribute("sql_query", str(stmt))
            return await db.scalar(stmt)
